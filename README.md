<h1>Welcome to Diavolo</h1>

Diavolo AKA Simple-bot is a discord bot project that is made to learn and be fun. A lot of the commands and way of setting up things inside Diavolo might not make sense. That is fine, the project and its entirety is a fun little side project for me the creator. If you find anything useful here that is great! If this looks like a mess that is also fine, because this isn't made for you. Anyway theres lots of fun and learning to be had. Any issues you find you can let me know, no idea when I will fix them though. You can also submit a merge request/pull request/code change, and I may get around to incorporating it. If none of the readme makes sense then you can try to explain it better!

<h3>Contributing</h3>
You must allow prettier and the linter to automatically style and fix up your code. You can es-lint disable as you see fit, but the prettier styling must be followed.

<h3>The config setup</h3>
To run add a config.json to the rootdir with the following parameters:

```js
{
    "token": "discord token",
    // <array of strings to call the bot with i.e. !>
    "prefixes": ["!", "<#1234123412341234>"],
    // sonarr config
    "sonarr": {
        "baseEndpoint": "http://<url>/sonarr/api/v3",
        "apiKey": "sonarr api key",
        // <list of string channel ids sonarr commands will work in>
        "approvedChannels": [
            "1234123412341234"
        ]
    },

    // list of servers to allow link embed fixing, if the server
    // is not in the list the links will not be fixed up
    "fixedServers": ["123196253412341234", "523196253412341234"],

    // essentially mandatory you need mongo to run a good chunk of commands
    "mongoConfig": {
        "url": "monogo url",
        "user": "username",
        "pwd": "password",
        "db": "database name for .db"
    },
    "errorRoomName": "diavolo-testing-room",
    "cookie": "<youtube cookie for youtube-dl",
    "X-Youtube-Identity-Token": "<youtube identitiy token for youtube-dl>",
    "restrictions": {
        "serverId": ["commandName"],
        "123196253412341234": ["8ball"],
        "essentially an allow list of commands for any given server":"",
    },
    //deprecated
    "reddit": {
        "clientSecret": "reddit client secret",
        "clientId": "reddit client id",
        "userAgent": "user agent name",
        "username": "username",
        "password": "password"
    },

}
```

<h3>Setting up MongoDB With Portainer</h3>

After nuking my raspberry pi's SD card and needing to rebuild I was stuck having to setup mongodb in docker on a pi3. This proved to be super painful and since I want to be able to set this up again quickly in the future here's how I got docker mongodb setup to use with diavolo.

<h4>Portainer's Stack</h4>
Using the stack feature of portainer I just plugged the following in and created a quick mongodb with persistant storage.

```yaml
services:
    mongodb-community-server:
        container_name: mongodb
        restart: always
        ports:
            - 27017:27017
        environment:
            - MONGO_INITDB_ROOT_USERNAME=userredacted
            - MONGO_INITDB_ROOT_PASSWORD=passredacted
        volumes:
            - /home/user/mongodb_docker/data:/data/db
        image: mongo:4.4.13
```

That is it I think I may have had to do `docker pull mongo:4.4.13` through the terminal to have the image ready for portainer to use.

Access this was super easy just make sure your raspberry pi has a static ip (for example lets say its 10.0.0.10) and the mongo url is `mongodb://userredacted:passredacted@10.0.0.10:27017` you might want to use better usernames and passwords. You also shouldn't expose this to the internet as theres no security or anything like that. Anyway this sets up the mongodb for diavolo.

<h3> Creating a new command: </h3>

<h4>Understanding the message handler:</h4>

<h5>Reactions:</h5>
When a message comes through the system the first thing that occurs is checking if the messasge should be reacted to.

Currently we check:

-   If a server has any keywords that warrant a reaction
-   If the message is a candidate for `deep but not profound`

The reactions are returned as a result to be awaited on by the messageHandler caller.

<h5>Automatic Link Embed Fixing</h5>

Currently diavolo is able to take specific links and create an embed for those links. Diavolo currently supports:

-   Twitter/X
-   Instagram

Doing so Diavolo will also attempt to delete the embed on the original message (the link will remain). In order to do so Diavolo requires the permission to manage messages.

Link fixup does not happen on every server you have to include a list of servers in the config to allow link fixup.

To add new link fixups I recommend checking out the inspiration <a href="https://github.com/canaria3406/ermiana/blob/e9ddf74ed214f1b5901bd7f01123d13ec66d2f7e/src/regex/regexManager.js"> repository on github</a>

A lot of the work done by canaria3406 has been reused, cleaned up, and expanded upon in Diavolo.

-   Entry point: [linkFixer.js](src\core\linkHandlers\linkFixer.js)

    -   You really shouldn't need to modify the linkFixer.js though if your handler function needs extra parameters this is the location to pass it down. Keep in mind all handlers get the same parameters so order matters here.
        -   result: the matcher performed on the message contents to extract the link and any information from the link.
        -   message: the message object in it's entirety
        -   embedSendStrategy: a function that handles sending embeds to a channel the function takes the parameters message, embed, and an optional array of extraEmbeds. Message is just the discord message object, embed is the main built embed, extraEmbeds are for attaching multiple photos to one embed.

-   Regex mapper: [regexMapper.js](src\core\linkHandlers\regex\regexMapper.js)
    -   This is where the mapping occurs between a link and a handler. You want the first entry of the map to be a regex for the URL you are handling, and the value should be the handler.

<h5>Guards:</h5>
We only wish to process messages that fall under certain criteria:

-   We do not want to futher process messages from other bots
-   We only want to futher process messages that call our bot

Once we verify the message is not from a bot, and is calling our bot we can process the message.

<h5>Reply strategy:</h5>

The heart of message processing is the `replyStrategy` every command sets the stage for.

The `replyStrategy` is function that takes the following parameters:

```js
userReplyMention, strategyReplyFunction, appendStrategy, splitStrategy, sendStrategy;
```

| parameter             | description                                                                                  | default value | need the pass through? |
| --------------------- | -------------------------------------------------------------------------------------------- | ------------- | ---------------------- |
| userReplyMention      | the value of the user to mention in the append                                               | none          | yes                    |
| strategyReplyFunction | the function called with `args` that returns a reply and options                             | none          | yes                    |
| appendStrategy        | a function that dictates how to append the userReplyMention                                  | none          | yes                    |
| splitStrategy         | how to break up the reply value from strategyReplyFunction to stay under the character limit | none          | yes                    |
| sendStrategy          | how to handle sending the message                                                            | defaultSend   | no                     |

With the exception of `sendStrategy` all other parameters need to be provided and setup by the command.

Ultimately the `replyStrategy` generates a function that replys to the user's command.
The returned funtion executes the following:

```js
const result = await strategyReplyFunction(args);
const options = result?.options || {};
const reply = result?.reply || result;

const send = appendStrategy({ userReplyMention, reply });
const msgs = splitStrategy(send);
const m = await Promise.all(msgs.map((msg) => sendStrategy({ message, content: msg, options })));
return m;
```

First it calls the provided `strategyReplyFunction` generating the result from the command's handler.

From which `options`, and `reply` are separated.

The reply then goes through the `appendStrategy` provided.

After the `appendStrategy` we call the `splitStrategy` to generate an array of strings to send.

Finally all messages are handled by mapping over them and calling the `sendStrategy`.

The defauilt `sendStrategy` is incredibly simple utilizing the channel the message originated in to send back a reponse.

```js
const defaultSend = ({ message, content = '', options = {} }) => message.channel.send({ content, ...options });
```

<h5>Determining strategy and preparing args</h5>

The next steps that occur are prepping values to inject into the strategyConstructor provided by the function we may call.

To make function creation easier the following values are provided to every single strategy constructor provided:

| parameter                | value                                                                                                        |
| ------------------------ | ------------------------------------------------------------------------------------------------------------ |
| replyStrategy            | The higher level function from earlier                                                                       |
| replyStrategyWithOptions | The same higher level function (deprecated)                                                                  |
| userReplyMention         | The formatted userId of the message sender                                                                   |
| message                  | The discord message object                                                                                   |
| args                     | An array of string after the trigger word                                                                    |
| appendUserMentionToFront | A function that can be utilized as appendStrategy, appends the given userReplyMention infront of the message |
| long2000Characters       | A function that splits messages into chunks of 2000 characters                                               |
| defaultSend              | The simple send function, provided for completeness                                                          |
| id                       | The raw discord id of the user who sent the message                                                          |
| serverId                 | The id of the server the message came from                                                                   |
| channel                  | The discord channel object of where the message originated                                                   |

Every strategy function constructor is provided these paramteters in an object.

After this object is established we determine which strategyFunctionConstructor we need to use based on priority:

1. If an internal function exists with the mapping from the trigger word use that if it does not exist go to 2
2. Check if a custom created command exists for the server, if it does not exist go to 3
3. Use the default strategy

Once we have decided which `strategyFunctionConstructor` to use we call it with `constructionArgs` to generate a `strategyFunction`.

The `strategyFunction` is then called with `args` from earlier and await the results.

<h4>Setting up a new function</h4>

Every single command needs to implement a few things.

1. A strategyReplyFunction
2. A strategy function constructor
3. A list of trigger words
4. A list of mappings from trigger words to strategy function constructor

Let us look at the handler function. The handler function you create accepts the array of args.

Note about the array of `args`: inside the strategy function contructor provide `args` can be modified to include any values your `strategyReplyFunction` may need.

Example from `./src/commands/yoink/yoink.js`:

```js
const yoink = async (args) => {
    const [textchannel] = args;
    const messages = await textchannel.messages.fetch({ limit: 5 });
    const stickerURLs = messages.reduce((acc, message) => {
        const url = message?.stickers?.values()?.next()?.value?.url;
        if (url) {
            acc.push(url);
        }
        return acc;
    }, []);

    if (stickerURLs.length === 0) {
        return 'Did not find sticker in last 5 messages';
    }
    return stickerURLs.join('\n');
};
```

A moment ago `args` was established as the list of strings that occur after the trigger word name. But as you can see here we are using a channel. How is this possible?

We take a look at `yoinkConstruct` which is our strategy function constructor.

```js
const yoinkConstruct = ({ args, channel, id, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    args.unshift(channel);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: yoink,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
```

They very first line we are unshifting the `channel` value, which was injected by the message handler, into args. Doing so allows the `strategyReplyFunction` to utilize the value. Notice how we take the injected value `replyStrategy` and call it with the parameters `replyStrategy` requires. We also leverage the injected value `appendUserMentionToFront` as our `appendStrategy`, our created function `yoink` as the `strategyReplyFunction`, and since we know our message never exceeds the character limit we provide a simple custom splitStrategy.

Now that our strategy function construct is completed we can look at the trigger words for our function.

```js
const yoinkTriggers = ['yoink'];
```

For ease of use we make our triggers a list of strings that can be used to call this command. Now that our list is completed we can now setup the mappings. Simply put all mappings are of the form:

```js
//generic mappings object
{
    [triggerWord1]: strategyFunctionConstructor,
    [triggerWord2]: strategyFunctionConstructor,
    [triggerWord3]: strategyFunctionConstructor
}
```

We can programatically generate this list with a reduce on the trigger words:

```js
const yoinkMappings = yoinkTriggers.reduce((acc, key) => {
    acc[key] = yoinkConstruct;
    return acc;
}, {});
```

Once we have our mappings in our proper form we need to export the mappings to use.

```js
module.exports = {
    yoink,
    mappings
};
```

We also export the strategy reply function but this is not necessary.

<h4>Incorporating your mappings into strategyMappings</h4>

In order for the bot to know about the new commands we must include them in `./src/core/strategyMappings.js`

Simply put the `strategyMappings` file imports your command's mappings, and exports them in one JSON.

At the top of the file add a require for the function's mappings (we will use `yoink.js` as an example)

```js
// ./src/core/strategyMappings.js
const { mappings: yoinkMappings } = require('../commands/yoink/yoink');
```

The inside module.exports spread the mappings:

```js
// ./src/core/strategyMappings.js

module.exports = {
    // there are many other exports in here
    ...yoinkMappings
};
```

And that is it. The new command is live with the trigger words after you start up the bot. The next section will help with debugging!

<h3>Docker Support</h3>
Getting tired of figuring out how to run the bot on startup a docker build process was created!
</br></br>

Simply run `npm run build` to build the image. The created image is tagged with `simple-bot:latest`. Personally I used portainer to manager and run my containers, you can do this with the cli just as easy. Running the container is left as an exercise for the developer, but docker is just one way to run the bot you can still use `npm run start` to get it running regardless, or `npm run start:hl` to have diavolo run as its own background process with logging to a file.

<h3>Debugging</h3>

To debug:

-   VS vode debugger should identify the launch.json and clicking the run button should launch the program in debug mode.
-   You can set break points which makes it super handy
