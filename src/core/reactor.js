/*
    {
        guildId: <string of the guild id>,
        reactMappings: {
            [string]: emoji
        }
    }
*/
const { getClient } = require('../utils/mongoTools/openConnection');
const CacheManager = require('../utils/cacheManager/inMemoryCacheManager');
const cacheManager = new CacheManager();
const reactor = async (message) => {
    //get the guild id
    const {
        guild: { id: serverId }
    } = message;
    //fetch reactor config from mongo
    const dbClient = await getClient();
    let reactMappings = {};
    try {
        const { item, expired } = cacheManager.getCachedItem(serverId);
        if (expired) {
            // need to refresh the cache item
            //this is where we query!
            //console.log('attempting to connect to reactor collection');
            const keywordsCol = await dbClient.collection('reactor');

            //check the server ID of the message
            if (!serverId) {
                //unable to determine server message came from
                return false;
            }
            const dbFilter = {
                guildId: serverId
            };

            [{ reactMappings } = { reactMappings: {} }] = await keywordsCol
                .find(dbFilter)
                .limit(1)
                .toArray();
            cacheManager.registerCacheItem(serverId, reactMappings);
        } else {
            //console.log('using cache for reactor');
            reactMappings = item;
        }
    } catch (e) {
        return false;
    } finally {
        dbClient.close();
    }

    //loop through each word in the message content and see if it needs a react, then build a reactor chain
    const words = message.content.toLowerCase();
    Object.keys(reactMappings).reduce(async (acc, key) => {
        await acc;
        if (words.includes(key)) {
            await message.react(reactMappings[key]);
        }
        return acc;
    }, []);
    return true;
};

module.exports = { reactor };
