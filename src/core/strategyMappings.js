const { mappings: thumbsupMappings } = require('../commands/thumbsup/thumbsup');
const { mappings: supportMappings } = require('../commands/thumbsup/topSupporters');
const { mappings: numberOfThumbsupsMappings } = require('../commands/thumbsup/numberOfThumbsup');
const { mappings: rollMappings } = require('../commands/roll/roll');
const { mappings: reminderMappings } = require('../commands/remindme/remindme');
const { mappings: pickitMappings } = require('../commands/pickit/pickit');
const { mappings: emojiMappings } = require('../commands/emoji/toEmoji');
const { mappings: _8ballMappings } = require('../commands/8ball/8ball');
const { mappings: domainMappings } = require('../commands/domain/statics');
const { mappings: congratsMappings } = require('../commands/congrats/congrats');
const { mappings: useast1Mappings } = require('../commands/useast1/useast1');
const { mappings: commandMappings } = require('../commands/commandmaker/commandmaker');
const { mappings: lookupMappings } = require('../commands/sonarr/lookup');
const { mappings: addSeriesMappings } = require('../commands/sonarr/series');
const { mappings: analyticsMappings } = require('../commands/analytics/analyticsMappings');
const { mappings: yoinkMappings } = require('../commands/yoink/yoink');
const { mappings: fehUnitMappings } = require('../commands/fehHeroStats/fehHeroStats');
const { mappings: randomUnitMappings } = require('../commands/fehHeroStats/randomHero');
const { mappings: getCatMappings } = require('../commands/cat/getCat');
const { mappings: catBallMappings } = require('../commands/cat/catBall');
const { mappings: goodMorbing } = require('../commands/goodmorbing/goodmorbing');
module.exports = {
    ...thumbsupMappings,
    ...supportMappings,
    ...numberOfThumbsupsMappings,
    ...rollMappings,
    ...reminderMappings,
    ...pickitMappings,
    ...emojiMappings,
    ...domainMappings,
    ..._8ballMappings,
    ...congratsMappings,
    ...useast1Mappings,
    ...commandMappings,
    ...lookupMappings,
    ...addSeriesMappings,
    ...analyticsMappings,
    ...yoinkMappings,
    ...fehUnitMappings,
    ...randomUnitMappings,
    ...getCatMappings,
    ...catBallMappings,
    ...goodMorbing
};
