const { getUserResponseStrategy } = require('../commands/commandmaker/commandmaker');
const { reactor } = require('./reactor');
const { ruleCheck } = require('./ruleCheck');
const { linkFixer } = require('./linkHandlers/linkFixer');
const appendUserMentionToFront = ({ userReplyMention, reply }) => `${userReplyMention} ${reply}`;

const long2000Characters = (message) => {
    const splits = Math.ceil(message.length / 2000);
    const msgs = [];
    for (let i = 0; i < splits; i++) {
        msgs.push(message.slice(i * 2000, i * 2000 + 2000));
    }
    return msgs;
};

const urlFixerSend = async (message, embed, extraEmbeds = []) => {
    await message.reply({ embeds: [embed, ...extraEmbeds], allowedMentions: { repliedUser: false } });
};

const defaultSend = ({ message, content = '', options = {} }) => message.channel.send({ content, ...options });
//things to do make a builder
const messageHandler = async (message, config, debugRoom, strategyMappings) => {
    //check if message has feet and react with honka <:honka:979794923551346749>
    const reactionPromises = await Promise.all([reactor(message), ruleCheck(message)]);

    //ignore bots
    if (message.author.bot) {
        return reactionPromises;
    }
    // check if the link needs to be fixed:

    // pass the content into the link fixer this will automatically fix links
    if (config?.fixedServers?.includes(message.guild.id)) {
        const fixed = await linkFixer(message, urlFixerSend);
        // we fixed the link and continue
        if (fixed) return true;
    }
    //ignore messages with no prefix
    if (!config.prefixes.some((prefix) => message.content.toLowerCase().indexOf(prefix) === 0)) {
        return reactionPromises;
    }
    const { restrictions } = config;
    const startTime = Date.now();
    const {
        guild: { id: serverId },
        author: { id },
        channel
    } = message;

    /*
        I guess I should explain replyStrategy and replyStrategyWithOptions:
        These are used by the commands to generated a strategyFunction that is used to handle messages and responses.
        The clever thing here is the implementation of the command sets the stage and then calls this function providing
        all the arguments and stuff necessary to generate a message.

        I guess the flow of a command:
        0. user makes a call with command <args>
        1. Command is identified
        2. The command has setup a function which the mappings returns (mappings[command] is a function)
        3. The function from 2 takes in predetermined construction args which include a bunch of handy default functions
           and context
        4. The constructor function returns a the result of replyStrategy.
        5. The handler then calls the result of the construciton function provided by the command
        6. The function generates and sends message to the expected channel.
    */
    const replyStrategy = ({
        userReplyMention,
        strategyReplyFunction,
        appendStrategy,
        splitStrategy,
        sendStrategy = defaultSend
    }) => async (args) => {
        //this function can either return just a string or an object of {reply, options}
        const result = await strategyReplyFunction(args);
        const options = result?.options || {};
        const reply = result?.reply || result;

        const send = appendStrategy({ userReplyMention, reply });
        const msgs = splitStrategy(send);
        const m = await Promise.all(msgs.map((msg) => sendStrategy({ message, content: msg, options })));
        return m;
    };
    //now this is where the fun begins
    const userReplyMention = `<@${id}>`;

    const [, ...args] = message.content.trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    const strategyFunctionConstructor = strategyMappings[command];
    if (restrictions[serverId]) {
        //restrictions exist initiate gatekeeping
        const gate = restrictions[serverId].some((allowed) => allowed === command);
        if (!gate) return false;
    }
    let strategyFunction = null;
    //create construction args which is supplied to every constructor function
    // NOTE: If you need more information from this level you can pass it through the construction args
    //       and extract it for use in your command's constructor implementation.
    const constructionArgs = {
        replyStrategy,
        //legacy from when they were separate functions
        replyStrategyWithOptions: replyStrategy,
        userReplyMention,
        message,
        args,
        appendUserMentionToFront,
        long2000Characters,
        defaultSend,
        id,
        serverId,
        channel
    };
    /*
        This is only here because I can
        BEHOLD THE ULTIMATE FUNCTION SELECTOR
    */
    strategyFunction = (strategyFunctionConstructor ||
        (await getUserResponseStrategy(command, serverId)) ||
        strategyMappings.default)(constructionArgs);

    await strategyFunction(args);
    const endTime = Date.now();
    //console.log(`Message processed total time: ${endTime - startTime}ms`);
    return reactionPromises;
};

module.exports = {
    messageHandler
};
