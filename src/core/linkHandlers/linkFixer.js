const { regexMap } = require('./regex/regexMapper');

//hacking this for now will work on a better solution

const linkFixer = async (message, embedSendStrategy) => {
    // uhhh what do we need here?
    if (/http/.test(message.content)) {
        for (const [regex, handler] of regexMap) {
            if (regex.test(message.content)) {
                const result = message.content.match(regex);
                await handler(result, message, embedSendStrategy);
                break;
            }
        }
        return true;
    }
    return false;
};

module.exports = {
    linkFixer
};
