const embedBeGone = async (message) => {
    try {
        if (message.deletable) {
            await message.suppressEmbeds(true);
        }
    } catch {
        // pretty much just want to avoid dealing w
        // console.log('no permission');
        throw new Error(`Unable to delete embed`);
    }
};

module.exports = { embedBeGone };
