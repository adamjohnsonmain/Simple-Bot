/* eslint-disable no-unused-expressions */
const fetch = require('node-fetch');
const { EmbedBuilder } = require('discord.js');
const { embedBeGone } = require('./embedBeGone');
// custom sender to just send the video link to the channel
const videoLinkSender = async (message, videoLink, quoted = false) => {
    await message.channel.send(`[${quoted ? 'Quoted ' : ''}Video](${videoLink})`);
};

// generates the proper video link in the event we have a video item
const videoLinkFormat = (link) => {
    const linkmatch = link.match(/https:\/\/.*?\.mp4/);
    if (linkmatch) {
        return linkmatch[0].includes('ext_tw_video') ? `${linkmatch[0]}?s=19` : linkmatch[0];
    } else {
        return 'https://twitter.com/';
    }
};

// usage handleImages(fxapitwitterEmbed, fxapiResp);
const handleImages = (embedContent, response) => {
    // first thing is first determine if we should handle multiple images or singular image
    const url = response?.data?.tweet?.url;
    if (url && url.length) {
        embedContent.setURL(url);
    }
    // mosaic case for no url multiple embed drifting
    if (!url && response.data.tweet?.media?.mosaic?.type === 'mosaic_photo') {
        embedContent.setImage(response.data.tweet.media.mosaic?.formats?.jpeg);
        return [];
    }
    // surgically extract photos
    const {
        data: {
            tweet: {
                quote: { media: { photos: quotePhotos = [] } = {} } = {},
                media: { photos: tweetPhotos = [] } = {}
            } = {}
        } = {}
    } = response;

    const photos = tweetPhotos.length ? tweetPhotos : quotePhotos;

    // for multiple images to work you have to send multiple embeds with the same url propery
    if (photos.length) {
        const [photo, ...rest] = photos;
        if (photo.type === 'photo') {
            embedContent.setImage(`${photo.url}?name=large`);
        }
        // THIS IS MY CODE I CAN CALL OVERFLOW PHOTOS OVERFLOWTOS
        return rest
            .map((overflowto) => {
                if (overflowto.type === 'photo') {
                    const overFlowImageEmbed = new EmbedBuilder();
                    overFlowImageEmbed.setURL(url);
                    overFlowImageEmbed.setImage(`${overflowto.url}?name=large`);
                    return overFlowImageEmbed;
                }
                return null;
            })
            .reduce((acc, next) => {
                if (next) {
                    acc.push(next);
                }
                return acc;
            }, []);
    }

    return [];
};

const quoteRetweetHandler = (embedContent, response) => {
    // check if QRT is present
    if (response?.data?.tweet?.quote?.id) {
        // we have a qrt to deal with
        const {
            data: {
                tweet: { quote }
            }
        } = response;
        // generate the quote author
        const { author = {} } = quote;
        const field = {};
        if (author?.screen_name) {
            field.name = `Quote retweet from: ${author?.screen_name}`;
            // include the QRT statistics for funsies
            const qrtInfo = `💬${quote.replies?.toString() || '0'} 🔁${quote.retweets?.toString() ||
                '0'} ❤️${quote.likes?.toString() || '0'}`;
            // value of the field is the tweet text, a link to the QRT
            field.value = `${quote.text}\n[link to retweet](${quote.url})\n${qrtInfo}`;
        }
        embedContent.addFields([field]);
    }
};

const generateAuthor = (response) => {
    // how many layers of defaulting are you on?
    const { data: { tweet: { author: { screen_name, avatar_url } = {} } = {} } = {} } = response;
    const result = {};
    if (screen_name) {
        result.name = `@${screen_name}`;
    }
    if (avatar_url) {
        result.iconURL = avatar_url;
    }
    return result;
};

const handleTwitterRegex = async (result, message, sender) => {
    const tid = result[1];
    try {
        // use fxtwitter api
        const rawSauce = await fetch(`https://api.fxtwitter.com/i/status/${tid}`);
        if (rawSauce.status === 200) {
            // set the fxapiResp which the original code expects
            const data = await rawSauce.json();
            const fxapiResp = { data };
            // setup the builder
            const fxapitwitterEmbed = new EmbedBuilder();
            // i dunno this color was preset
            fxapitwitterEmbed.setColor(0x1da1f2);
            // set the author
            fxapitwitterEmbed.setAuthor(generateAuthor(fxapiResp));

            // set the title
            fxapitwitterEmbed.setTitle(fxapiResp?.data?.tweet?.author?.name ?? 'Twitter.com');

            // something something tweet url
            fxapitwitterEmbed.setURL(fxapiResp?.data?.tweet?.url ?? `https://twitter.com/i/status/${tid}`);
            if (fxapiResp?.data?.tweet?.text && fxapiResp?.data?.tweet?.text.length) {
                fxapitwitterEmbed.setDescription(fxapiResp?.data?.tweet?.text);
            }

            // handle any potential Quote Retweets
            quoteRetweetHandler(fxapitwitterEmbed, fxapiResp);

            // handle the images from the tweet
            const extraEmbeds = handleImages(fxapitwitterEmbed, fxapiResp);

            fxapitwitterEmbed.setTimestamp(fxapiResp?.data?.tweet?.created_timestamp * 1000);
            // craft the footer info
            const fxapitweetinfo = `💬${fxapiResp.data.tweet.replies?.toString() ||
                '0'} 🔁${fxapiResp.data.tweet.retweets?.toString() || '0'} ❤️${fxapiResp.data.tweet.likes?.toString() ||
                '0'}`;
            fxapitwitterEmbed.setFooter({ text: fxapitweetinfo });
            await sender(message, fxapitwitterEmbed, extraEmbeds);

            let sentVideo = false;
            // send a link to the video immediately afterwards which kinda sucks
            // TODO: investigate a way to embed video link in the embedded content
            fxapiResp?.data?.tweet?.media?.all.forEach((element) => {
                if (element.type !== 'photo') {
                    videoLinkSender(message, videoLinkFormat(element.url));
                    sentVideo = true;
                }
            });
            if (!sentVideo) {
                // go through the media for the quote reply
                fxapiResp?.data?.tweet?.quote?.media?.all?.forEach((element) => {
                    if (element.type !== 'photo') {
                        videoLinkSender(message, videoLinkFormat(element.url), true);
                        sentVideo = true;
                    }
                });
            }
            //finally remove the original embed
            await embedBeGone(message);
        } else {
            throw new Error(`fxtwitter api error: ${tid}`);
        }
    } catch (e) {
        console.log(e);
    }
};
module.exports = { handleTwitterRegex };
