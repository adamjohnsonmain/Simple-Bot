const { embedBeGone } = require('./embedBeGone');

const handleInstagramRegex = async (result, message) => {
    //remove embed if possible
    await embedBeGone(message);

    await message.reply({
        content: `https://www.instagramez.com/p/${result[1]}/`,
        allowedMetions: { repliedUser: false }
    });
};

module.exports = { handleInstagramRegex };
