const { EmbedBuilder } = require('discord.js');
const fetch = require('node-fetch');
const { embedBeGone } = require('./embedBeGone');

// custom sender to just send the video link to the channel
const videoLinkSender = async (message, videoLink) => {
    await message.channel.send(`[Video](${videoLink})`);
};
/*
https://bsky.social/xrpc/com.atproto.sync.getBlob?cid=bafkreihqki2ckcbpzinpnyhlintgw7mbjbvpb326i4ejthio7wlnhrx2uu&did=did:plc:t6ubj2wlhc34awzcymh3qpur
*/
const handleVideoLink = async (message, response) => {
    // two possible video locations
    //   1. on the main thread
    //   2. on the quote thread
    let videoInfo = null;
    let did = response.thread?.post?.author.did;
    // basic case a post has a video
    if (response.thread.post?.embed?.$type === 'app.bsky.embed.video#view') {
        videoInfo = response.thread.post.embed;
    } else if (response.thread?.post?.embed?.$type === 'app.bsky.embed.record#view') {
        //quote case video checking
        if (response.thread?.post?.embed?.record?.embeds?.[0]?.$type === 'app.bsky.embed.video#view') {
            // the embed has a video we can handle this
            videoInfo = response.thread?.post?.embed?.record?.embeds?.[0];
            did = response.thread?.post?.embed?.record?.author.did;
        }
    } else if (response.thread.post?.embed?.$type === 'app.bsky.embed.recordWithMedia#view') {
        // more mixed media to deal with
        if (response.thread.post?.embed?.media?.$type === 'app.bsky.embed.video#view') {
            // main thread has a video use that
            videoInfo = response.thread.post?.embed?.media;
        } else if (response.thread.post?.embed?.record?.record?.embeds?.[0]?.$type === 'app.bsky.embed.video#view') {
            // quote has a video use that
            videoInfo = response.thread.post?.embed?.record?.record?.embeds?.[0];
            did = response.thread.post?.embed?.record?.record?.author.did;
        }
    }
    // we may have video info
    if (videoInfo) {
        // we may have a video object
        if (videoInfo.$type === 'app.bsky.embed.video#view') {
            // we have a video
            const url = `https://bsky.social/xrpc/com.atproto.sync.getBlob?cid=${videoInfo.cid}&did=${did}`;
            await videoLinkSender(message, url);
        }
    }
};

const handleImages = (embedContent, images, mainURL) => {
    const [image, ...rest] = images;
    // threadResp.data.thread.post.embed.images[0].fullsize
    embedContent.setImage(image.fullsize);
    return rest.map((overFlowImage) => {
        const overFlowImageEmbed = new EmbedBuilder();
        overFlowImageEmbed.setURL(mainURL);
        overFlowImageEmbed.setImage(overFlowImage.fullsize);
        return overFlowImageEmbed;
    });
};

const quoteHandler = (embedContent, quoteRecord) => {
    let useRecord = quoteRecord;
    // handle the case where record is nested once more for
    // mixed media
    if (useRecord?.record?.$type === 'app.bsky.embed.record#viewRecord') {
        useRecord = quoteRecord.record;
    }
    const quoteAuthor = useRecord.author.displayName;
    const quoteInfo = `💬${useRecord.replyCount.toString()} 🔁${useRecord.repostCount.toString()} ❤️${useRecord.likeCount.toString()}`;

    const quoteText = `${useRecord.value.text}\n${quoteInfo}`;
    const field = {
        name: `Quoted From: ${quoteAuthor}`,
        value: quoteText
    };
    embedContent.addFields([field]);
};

const handleBlueskyRegex = async (result, message, sender) => {
    try {
        const didResp = await fetch(`https://bsky.social/xrpc/com.atproto.identity.resolveHandle?handle=${result[1]}`);
        const didRespData = await didResp.json();
        if (didResp.status === 200) {
            const bskyUrl = `https://public.api.bsky.app/xrpc/app.bsky.feed.getPostThread?uri=at://${
                didRespData.did
            }/app.bsky.feed.post/${result[2]}`;
            const threadResp = await fetch(bskyUrl);
            const threadRespData = await threadResp.json();
            if (threadResp.status === 200) {
                const blueskyEmbed = new EmbedBuilder();
                if (threadRespData.thread?.post?.author?.avatar) {
                    blueskyEmbed.setAuthor({
                        name: `@${result[1]}`,
                        iconURL: threadRespData.thread.post.author.avatar
                    });
                } else {
                    blueskyEmbed.setAuthor({ name: `@${result[1]}` });
                }

                if (threadRespData.thread?.post?.author?.displayName) {
                    blueskyEmbed.setTitle(threadRespData.thread.post.author.displayName);
                    blueskyEmbed.setURL(`https://bsky.app/profile/${result[1]}/post/${result[2]}`);
                } else {
                    blueskyEmbed.setTitle(result[1]);
                    blueskyEmbed.setURL(`https://bsky.app/profile/${result[1]}/post/${result[2]}`);
                }

                if (threadRespData.thread?.post?.record?.text) {
                    blueskyEmbed.setDescription(threadRespData.thread.post.record.text);
                }

                // hand quotes
                if (threadRespData.thread.post.embed?.record) {
                    // need to handle QRT
                    quoteHandler(blueskyEmbed, threadRespData.thread.post.embed.record);
                }
                let extraEmbeds = [];
                if (
                    threadRespData.thread.post.embed?.images ||
                    threadRespData.thread.post.embed?.record?.embeds?.[0]?.images
                ) {
                    const images =
                        threadRespData.thread.post.embed?.images ??
                        threadRespData.thread.post.embed?.record?.embeds?.[0]?.images;
                    extraEmbeds = handleImages(
                        blueskyEmbed,
                        images,
                        `https://bsky.app/profile/${result[1]}/post/${result[2]}`
                    );
                }
                const threadinfo = `💬${threadRespData.thread.post.replyCount.toString()} 🔁${threadRespData.thread.post.repostCount.toString()} ❤️${threadRespData.thread.post.likeCount.toString()}`;
                blueskyEmbed.setFooter({ text: threadinfo });
                await sender(message, blueskyEmbed, extraEmbeds);
                // do we have to handle a video?
                handleVideoLink(message, threadRespData);
                //finally remove the original embed
                await embedBeGone(message);
            }
        }
    } catch (e) {
        throw new Error(`Blue sky link handler blew up: ${e.message}`);
    }
};
module.exports = { handleBlueskyRegex };
