// this will probably be a map of regex -> linkHandlers

const { handleTwitterRegex } = require('./linkHandlers/handleTwitterRegex');
const { handleInstagramRegex } = require('./linkHandlers/handleInstagramRegex');
const { handleBlueskyRegex } = require('./linkHandlers/handleBlueSkyRegex');

// https://github.com/canaria3406/ermiana/blob/e9ddf74ed214f1b5901bd7f01123d13ec66d2f7e/src/regex/regexManager.js
const regexMap = new Map([
    [/https:\/\/x\.com\/[A-Za-z0-9_]{1,15}\/status\/([0-9]+)/, handleTwitterRegex],
    [/https:\/\/twitter\.com\/[A-Za-z0-9_]{1,15}\/status\/([0-9]+)/, handleTwitterRegex],
    [/https:\/\/www\.instagram\.com\/(?:p|reel|reels)\/([a-zA-Z0-9-_]+)/, handleInstagramRegex],
    [/https:\/\/www\.instagram\.com\/[A-Za-z0-9_.]+\/(?:p|reel)\/([a-zA-Z0-9-_]+)/, handleInstagramRegex],
    [/https:\/\/bsky\.app\/profile\/([a-zA-Z0-9-.]+)\/post\/([a-zA-Z0-9]{10,16})/, handleBlueskyRegex]
]);

module.exports = { regexMap };
