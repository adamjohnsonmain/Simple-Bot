const XYSplitter = ' but not ';
const xMatcher = /([a-zA-Z0-9])\1+/;
const ruleCheck = async (message) => {
    // eslint-disable-next-line no-control-regex
    const clean = message.content.replace(/[^\x00-\x7F]/g, ' ');
    const firstSplit = clean
        .toLowerCase()
        .trim()
        .split(XYSplitter);
    if (firstSplit.length === 1) {
        return 0;
    }
    const bits = firstSplit.map((z) => z.split(' ').filter((v) => v)).flat();
    if (bits.length !== 2) {
        return 0;
    }
    const [x, y] = bits;
    const react = x.match(xMatcher) && !y.match(xMatcher) ? '👍' : '👎';
    await message.react(react);

    return 0;
};

module.exports = { ruleCheck };
