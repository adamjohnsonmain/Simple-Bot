// TODO: Investigate if this is necessary with the PI3
global.AbortController = require('abort-controller');

// Discord APIs
const Discord = require('discord.js');
const { GatewayIntentBits } = Discord;

// Config File Import
const config = require('../config.json');

// CORE Systems
const { messageHandler } = require('./core/message-handler');
const strategyMappings = require('./core/strategyMappings');

// Managers

// Setup the Client with the necessary Intents
const client = new Discord.Client({
    intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.GuildVoiceStates,
        GatewayIntentBits.MessageContent
    ]
});
client.login(config.token);

let errorLoggingRoom;
/*
    On ready we want to log out that the bot is up and running.
    We also want to setup any other initial states of the bot.
    Previously this was a good place to setup any runners we had.
*/
client.on('ready', () => {
    console.log(
        `Bot has started, with ${client.users.cache.size} users, in ${client.channels.cache.size}\ 
        channels of ${client.guilds.cache.size} guilds.`
    );
    errorLoggingRoom = client.channels.cache.find((a) => a.name === config.errorRoomName);
    client.user.setActivity('The boss');
});

/*
    The main handler. When a message is created we call our message handler along with the strategy mappings
    for all the code based commands.
*/
client.on('messageCreate', async (message) => {
    try {
        await messageHandler(message, config, errorLoggingRoom, strategyMappings);
    } catch (error) {
        await errorLoggingRoom.send(`ERROR: ${error.message}`);
    }
});

/*
    The bot also handles updates in case the user messes up a command. We do not process any edits that do
    not change anything in the message. The logic is the same as message create if the message is different.
*/
client.on('messageUpdate', async (oldMessage, newMessage) => {
    if (newMessage.content !== oldMessage.content) {
        try {
            await messageHandler(newMessage, config, errorLoggingRoom, strategyMappings);
        } catch (error) {
            await errorLoggingRoom.send(`ERROR: ${error.message}`);
        }
    }
});

/*
    On errors we want to log the message then send a message to our error logging room.
*/
client.on('error', async (error) => {
    console.log(error.message);
    await errorLoggingRoom.send(error.message);
});
