//start off with importing on mongo config
const {
    mongoConfig: { url, db }
} = require('../../../config.json');
//get our mongo client
const { MongoClient } = require('mongodb');

/*
    you may need this at a later date itis the 2.2 docs
    https://mongodb.github.io/node-mongodb-native/2.2/api/Collection.html#updateOne
*/
/*
    this function will open a connection to the db on the config db
*/
const getClient = async () => {
    const client = new MongoClient(`${url}`);
    await client.connect();
    //console.log('Starting DB connection');
    const dbConnection = await client.db(db);
    //console.log('connected');
    dbConnection.close = () => client.close();
    return dbConnection;
};

module.exports = { getClient };
