const { promisify } = require('util');
const fs = require('fs');
const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);
const path = require('path');

const makeCacheUtilFunctions = (fileName) => {
    const cachedDataPath = path.join(__dirname, '../', 'cached_data', fileName);
    const readFromCache = async () => {
        let data = null;
        if (fs.existsSync(cachedDataPath)) {
            try {
                const thumbsupDataRaw = await readFileAsync(cachedDataPath, {
                    flag: 'r'
                });
                data = JSON.parse(thumbsupDataRaw);
            } catch (err) {
                return 'Unable to read from cached file';
            }
        } else {
            data = {};
        }
        return data;
    };
    const writeToCache = (dataToWrite) => {
        return writeFileAsync(cachedDataPath, dataToWrite, { mode: 0o777 });
    };
    return {
        readFromCache,
        writeToCache
    };
};

module.exports = {
    makeCacheUtilFunctions
};
