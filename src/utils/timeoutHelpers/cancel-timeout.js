/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-shadow */
function timeout(ms) {
    let timeout;

    const promise = new Promise((resolve) => {
        timeout = setTimeout(function() {
            resolve('timeout done');
        }, ms);
    });

    return {
        promise,
        cancel() {
            clearTimeout(timeout);
        }
    };
}

//essentially store this object (easy) and if the user cancels tthen it is cancelled then delete the id entry in the object
module.exports = { timeout };
