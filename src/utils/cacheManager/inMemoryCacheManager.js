/*
    The purpose of cache manager is to keep track of items in memory from database requests where caching makes sense

    cache: {
        [name]: {
            item: <cached value can be anything>,
            registered: Date when the item was added,
            expires: <time in ms when the item is expired>
        }
    }
 
*/
class CacheManager {
    // 10 minutes is our default expires time.
    DEFAULT_EXPIRES = 1000 * 10 * 60;
    constructor() {
        this.cache = {};
    }
    getCachedItem(cachedName) {
        // get the cached item
        const cachedItem = this.cache[cachedName];
        // if it exists we have something to do
        if (cachedItem) {
            const { item, registered, expires } = cachedItem;
            const expiryDate = registered + expires;
            // if expiryDate is surpassed by now then we return expired result
            if (expiryDate < Date.now()) {
                return { item: null, expired: true };
            } else {
                // the item is not expired, return the bundle
                return { item, expired: false };
            }
        }
        // nothing cached return false
        return {
            item: null,
            expired: true
        };
    }
    registerCacheItem(cachedName, cachedItem, options = { expiresIn: this.DEFAULT_EXPIRES }) {
        const { expiresIn } = options;
        this.cache[cachedName] = {
            item: cachedItem,
            registered: Date.now(),
            expires: expiresIn
        };
        return cachedItem;
    }
}
module.exports = CacheManager;
