class FunctionRunner {
    constructor() {
        this.functions = [];
        this.toggledOff = false;
    }
    register(newFuction, context) {
        this.functions.push({ funct: newFuction, context });
    }
    setStop() {
        this.toggledOff = true;
    }
    setStart() {
        this.toggledOff = false;
    }
    async runAll() {
        if (this.toggledOff) return true;
        await this.functions.reduce(async (acc, { funct, context }) => {
            await acc;
            return funct(context);
        }, Promise.resolve());
        return true;
    }
}

module.exports = { FunctionRunner };
