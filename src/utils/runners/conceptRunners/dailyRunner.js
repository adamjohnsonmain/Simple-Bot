//this is a collection of functions that should run at certain times during the day
/*
    this place is a mess i really dislike it
    behol
*/
const moment = require('moment');
const { intervalRunner } = require('../intervalRunner');
const { FunctionRunner } = require('../functionRunner');
const { setupBGDAlertFunct } = require('../../../reddit/boardgamedeal-alert');
const houredRunner = async ({ runner, hourTrigger, minuteTrigger = 0 }) => {
    const now = moment();
    if (now.hour() === hourTrigger && now.minutes() === minuteTrigger) {
        await runner.runAll();
        runner.setStop();
    } else if (runner.toggledOff) {
        runner.setStart();
    }
};

const setupSixPmDailyRunner = (client) => {
    const roomToSendMessge = client.channels.cache.find((a) => a.name === 'diavolo-testing-room');
    const simpleSixPmFunction = async (sendHere) => {
        await sendHere.send('It is 6pm');
    };
    const sixPMRunner = new FunctionRunner();
    sixPMRunner.register(simpleSixPmFunction, roomToSendMessge);
    return { funct: houredRunner, context: { runner: sixPMRunner, hourTrigger: 18 } };
};
const runAllReddit = async ({ runner }) => {
    await runner.runAll();
};
const setupBGDRunner = async (client, redditConnection) => {
    //fuck what is my context
    //get my room this will come back to haunt you later on
    const roomToSendMessge = client.channels.cache.find((a) => a.name === 'board-game-deals');
    const bgdAlertFunct = await setupBGDAlertFunct(
        redditConnection,
        roomToSendMessge,
        'Boardgamedeals',
        'bgdcheckedposts.json'
    );
    const bgdAlertRunner = new FunctionRunner();
    bgdAlertRunner.register(bgdAlertFunct);
    return { funct: runAllReddit, context: { runner: bgdAlertRunner } };
};

const setupBPDRunner = async (client, redditConnection) => {
    //fuck what is my context
    //get my room this will come back to haunt you later on
    const roomToSendMessge = client.channels.cache.find((a) => a.name === 'build-a-pc-sales');
    const bpdAlertFunct = await setupBGDAlertFunct(
        redditConnection,
        roomToSendMessge,
        'buildapcsales',
        'bpdcheckedposts.json'
    );
    const bpdAlertRunner = new FunctionRunner();
    bpdAlertRunner.register(bpdAlertFunct);
    return { funct: runAllReddit, context: { runner: bpdAlertRunner } };
};

const setupDailiesRunner = (client) => {
    const sixPM = setupSixPmDailyRunner(client);

    const ms = 1000;
    const functContextArray = [sixPM];

    const dailyIntervalTimer = intervalRunner(ms, functContextArray);
    return [dailyIntervalTimer];
};

const setupFireAlarmReminder = (client) => {
    const fireAlarmRoom = client.channels.cache.find((a) => a.name === 'weekly-fire-alarm-alert');
    const simpleAlert = async (sendHere) => {
        console.log('running and sending');
        const now = moment();
        // only run on sunday at 4pm or something i dunno
        if (now.hour() === 16 && now.day() === 0) {
            //to do update this to be an embeded message
            await sendHere.send('@everyone - Reminder to test your fire alarms. <insert patreon link here>');
        }
    };
    // hourly runner that checks for sunday
    const hourInMs = 1000 * 60 * 60;
    const hourlyCheckForSundayRunner = intervalRunner(
        hourInMs,
        [{ funct: simpleAlert, context: fireAlarmRoom }],
        () => {}
    );
    return hourlyCheckForSundayRunner;
};

const setupRedditRunner = async (client, redditConnection) => {
    const bgd = await setupBGDRunner(client, redditConnection);
    const bpd = await setupBPDRunner(client, redditConnection);
    //i think I need a 15 minute  interval for the interval timer
    const fifteen = 1000 * 60 * 5;
    const functContextArray = [bpd, bgd];
    const reddit15MinuteTimer = intervalRunner(fifteen, functContextArray);
    return [reddit15MinuteTimer];
};

module.exports = { setupDailiesRunner, setupRedditRunner, setupFireAlarmReminder };
