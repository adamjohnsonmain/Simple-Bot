const { promisify } = require('util');
const fs = require('fs');
const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);
const path = require('path');
const readCheckPosts = async (fileName) => {
    const cachedDataPath = path.join(__dirname, '../', 'cached_data', fileName);

    let checkedPosts = null;
    if (fs.existsSync(cachedDataPath)) {
        try {
            const checkedPostsDataRaw = await readFileAsync(cachedDataPath, {
                flag: 'r'
            });
            checkedPosts = JSON.parse(checkedPostsDataRaw);
        } catch (err) {
            return 'Sad days unable to parse the cached data';
        }
    } else {
        checkedPosts = [];
    }
    return checkedPosts;
};

const writeCheckPosts = (dataToWrite, fileName) => {
    const cachedDataPath = path.join(__dirname, '../', 'cached_data', fileName);

    return writeFileAsync(cachedDataPath, dataToWrite, { mode: 0o777 });
};
module.exports = {
    readCheckPosts,
    writeCheckPosts
};
