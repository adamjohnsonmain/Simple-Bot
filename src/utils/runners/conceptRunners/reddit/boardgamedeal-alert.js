//given a reddit instance created by snoowrap
const { normalizePost } = require('./post-normalizer');
const { constructUserMentions } = require('./parseBoardgamedealMentions');
const options = { limit: 10 };
const setupBGDAlertFunct = async (redditConnection, dealRoom, subreddit, fileName) => {
    //setup the connection on our subreddit
    const subredditConnection = await redditConnection.getSubreddit(subreddit);
    //what am I doing here
    const bgdNewScraperFunct = newewScraperFunctionMaker(subredditConnection, dealRoom, fileName, subreddit);
    return bgdNewScraperFunct;
};

//assume bgd is a subreddit instance created by redditConnection
const newewScraperFunctionMaker = (subredditConnect, dealRoom, fileName, subredditName) => async () => {
    const { readCheckPosts, writeCheckPosts } = require('./cache-reader');
    //get new posts
    const newPosts = await subredditConnect.getNew(options);
    //normalize them
    const normalizedNewPosts = newPosts.map((post) => normalizePost(post));
    //filter out posts that have been looked at alread
    //attempt to load the cache file
    const readCheckPostsData = await readCheckPosts(fileName);
    const filteredNewPosts = normalizedNewPosts.filter(({ id }) => !readCheckPostsData.includes(id));
    if (filteredNewPosts.length) {
        //we have some posts to parse through
        await filteredNewPosts.reduce(async (acc, { id, title, url }) => {
            await acc;
            //so now we need to send the link + url to a channel in discords :thonk: we might need some extra parameters
            //strategy find channel to send message to
            //send formatted message
            //determined who should be at mentioned
            const userMentions = await constructUserMentions(title, subredditName.toLowerCase());
            dealRoom.send(`${title}. Link: ${url} ${userMentions}`);
            readCheckPostsData.unshift(id);
        }, 0);
        //write out only 20 post ids to save space
        await writeCheckPosts(JSON.stringify(readCheckPostsData.slice(0, 20)), fileName);
    }
};
module.exports = { setupBGDAlertFunct };
