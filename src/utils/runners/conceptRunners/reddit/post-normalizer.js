const makeRedditLink = (permalink) => `https://old.reddit.com${permalink}`;

const normalizePost = ({ id, title, permalink }) => {
    return { id, title, url: makeRedditLink(permalink) };
};

module.exports = { normalizePost };
