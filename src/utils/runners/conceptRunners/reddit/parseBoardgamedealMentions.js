//okay we need to read the file
/*
      {
        'serverid': {
            'boardgamedeals': {
                userid: {
                    keywords: ['string 1', 'string 2'],

                    < ! Ignore this section for now this is planned threshold keyword ! >
                    pricePointKeywords: [  
                        {
                            threshold: 123412341,
                            keyword: 'string'
                        }
                    ]
                }
            }
        }

*/

const { getClient } = require('../../../mongoTools/openConnection');
const approvedServer = '263072386671181825';
const constructUserMentions = async (title, listName) => {
    //console.log(listName);
    //parse the file

    const dbClient = await getClient();
    let listOfServerKeywords = null;
    try {
        //this is where we query!
        //console.log('attempting to connect to keyword collection');
        const keywordsCol = await dbClient.collection('keywords');
        const dbFilter = {
            guildId: approvedServer,
            runnerName: listName
        };

        listOfServerKeywords = await keywordsCol.find(dbFilter).toArray();
    } catch (error) {
        throw error;
    } finally {
        dbClient.close();
    }
    if (!listOfServerKeywords) {
        return '';
    }
    /*
        list from mongo:
        [
            {
                userId: xyz,
                keywords: [xyz, abc, ...]
            }
        ]

        to simplify the process we want the data to look like:
        userid: {
           keywords: ['string 1', 'string 2']
        }
    */
    const userData = listOfServerKeywords.reduce((acc, { userId, keywords }) => {
        acc[userId] = { keywords };
        return acc;
    }, {});
    const userKeys = Object.keys(userData);
    if (!userKeys.length) {
        return '';
    }
    const userMentions = userKeys
        .map((userId) => {
            //check for keyword data
            const { keywords } = userData[userId];
            if (!keywords.length) {
                return '';
            }
            const lowerCaseTitle = title.toLowerCase();
            const match = keywords.reduce((acc, keyword) => lowerCaseTitle.includes(keyword) || acc, false);
            if (match) {
                return `<@${userId}>`;
            }
            return '';
        })
        .join(' ');
    return userMentions;
};

module.exports = { constructUserMentions };
