const { FunctionRunner } = require('./functionRunner');

function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

const defaultErrorHandler = (error) => console.log(error.message);

const intervalRunner = (ms, functContextArray, errorHandler = defaultErrorHandler) => {
    const runner = new FunctionRunner();
    functContextArray.forEach(({ funct, context }) => {
        runner.register(funct, context);
    });
    return async () => {
        const control = true;
        do {
            try {
                await sleep(ms);
                await runner.runAll();
            } catch (error) {
                errorHandler(error);
            }
        } while (control);
    };
};

module.exports = { intervalRunner };
