const fetch = require('node-fetch');

//cool so we need the API key from our config
const ADD_OPTIONS = {
    ignoreEpisodesWithFiles: true,
    ignoreEpisodesWithoutFiles: false,
    searchForMissingEpisodes: true
};
const {
    sonarr: { baseEndpoint, apiKey }
} = require('../../../config.json');

const LOOKUP_ENDPOINT = 'series/lookup';
const SERIES_ENDPOINT = 'series';

const lookupSeries = async (term) => {
    //need to check if the channel is danyarr
    //neat so we can now setup a get request to the endpoint
    /*
        todo
            - encode terms
            - build query parameters
            - make get request
            - parse results
            - transform results
    */
    const body = await lookupSeriesINTERNAL(term);
    if (body.length === 0) {
        return [];
    }
    const parsed = body.slice(0, 5).map(({ title, tvdbId, remotePoster, overview, added }) => {
        return {
            title,
            tvdbId,
            remotePoster,
            overview: overview ?? 'No overview found',
            seriesAdded: calcAdded(added)
        };
    });
    return parsed;
};
const calcAdded = (dateString) => {
    return dateString !== '0001-01-01T00:00:00Z';
};

const lookupSeriesINTERNAL = async (term) => {
    const encodedTerm = encodeURIComponent(term);
    const stringURL = `${baseEndpoint}/${LOOKUP_ENDPOINT}?term=${encodedTerm}&apikey=${apiKey}`;

    const result = await fetch(stringURL);
    const body = await result.json();
    return body;
};

const addSeries = async (id) => {
    //first lookup series by id
    const [showDetails] = await lookupSeriesINTERNAL(`tvdb:${id}`);
    //this lookup should be the show we want
    if (!showDetails) {
        //nothing was found
        return {
            success: false,
            message: 'Show was not found'
        };
    }

    const { title, tvdbId, titleSlug, images, seasons, qualityProfileId, languageProfileId } = showDetails;

    const profileId = 1;
    const monitored = true;
    const postBODY = {
        title,
        qualityProfileId: qualityProfileId || 1,
        languageProfileId: languageProfileId || 1,
        tvdbId,
        titleSlug,
        images,
        seasons,
        profileId,
        monitored,
        rootFolderPath: `E:\\Media Store\\Television - Anime`,
        seriesType: 'Anime',
        addOptions: ADD_OPTIONS
    };
    const stringURL = `${baseEndpoint}/${SERIES_ENDPOINT}?apikey=${apiKey}`;
    //in here we have our show found so we can start extracting information
    const res = await fetch(stringURL, {
        method: 'post',
        body: JSON.stringify(postBODY)
    });
    const resBody = await res.json();
    return {
        success: true,
        message:
            'series has been added, its in sonarrs hands now. Note: this is automatic if things are wrong after it is added reach out to my owner'
    };
};

module.exports = {
    lookupSeries,
    addSeries
};
