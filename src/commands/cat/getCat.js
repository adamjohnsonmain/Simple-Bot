const fetch = require('node-fetch');
const fs = require('fs');
const endpoint = 'https://cataas.com/cat';
const { helpRegister } = require('../domain/statics');

const getCat = async () => {
    const res = await fetch(endpoint);
    if (res.ok) {
        //this shoould be the image
        return {
            reply: 'Enjoy this cat',
            options: { files: [{ attachment: res.body }] }
        };
    } else {
        return {
            reply: 'There was an error sorry!'
        };
    }
};
const triggerWords = ['cat'];
helpRegister('cat', 'gives you a random cat');
const getCatConstruct = ({ args, id, appendUserMentionToFront, replyStrategyWithOptions, userReplyMention }) => {
    return replyStrategyWithOptions({
        userReplyMention,
        strategyReplyFunction: getCat,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const congratsMappings = triggerWords.reduce((acc, key) => {
    acc[key] = getCatConstruct;
    return acc;
}, {});

const mappings = { ...congratsMappings };
module.exports = {
    getCatConstruct,
    mappings
};
