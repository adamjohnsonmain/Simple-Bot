const fetch = require('node-fetch');
const fs = require('fs');
const baseEndpoint = 'https://cataas.com/cat';
const { helpRegister } = require('../domain/statics');
const { messages } = require('../8ball/8ballData');
const queryParamString = 'fontSize=33&fontColor=white';

const catBall = async () => {
    const limit = messages.length;
    const message = messages[Math.floor(Math.random() * limit)];
    const endpoint = `${baseEndpoint}/says/${message}?${queryParamString}`;
    const res = await fetch(endpoint);
    if (res.ok) {
        //this shoould be the image
        return {
            reply: 'Your cat wisdom',
            options: { files: [{ attachment: res.body }] }
        };
    } else {
        return {
            reply: `Something went wrong your text based cat wisdom: ${message}`
        };
    }
};
const triggerWords = ['catball'];
helpRegister('catball', 'gives you a random catball wisdom');
const catBallConstruct = ({ args, id, appendUserMentionToFront, replyStrategyWithOptions, userReplyMention }) => {
    return replyStrategyWithOptions({
        userReplyMention,
        strategyReplyFunction: catBall,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const congratsMappings = triggerWords.reduce((acc, key) => {
    acc[key] = catBallConstruct;
    return acc;
}, {});

const mappings = { ...congratsMappings };
module.exports = {
    catBallConstruct,
    mappings
};
