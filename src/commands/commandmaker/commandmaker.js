/* eslint-disable require-await */
const { helpRegister } = require('../domain/statics');
const { getClient } = require('../../utils/mongoTools/openConnection');
const addcommand = require('./addcommand/addcommand');
const listcommands = require('./listcommands/listcommands');
const listcommandnames = require('./listcommands/listcommandnames');
const deletecommand = require('./deletecommand/deletecommand');
/*
    {
        guildId: xxxx,
        commandName: xxx,
        commandResponse: xxx,
    }
*/

const noAppendStrategy = ({ reply }) => `${reply}`;

const getUserResponseStrategy = async (commandName, serverId) => {
    const dbClient = await getClient();
    try {
        //this is where we query!
        // console.log('attempting to connect to commands collection');
        const commandCol = await dbClient.collection('commands');

        const selector = {
            guildId: serverId
        };
        const response = await commandCol.find(selector).toArray();
        const { commandResponse } = response.find(({ commandName: c }) => c === commandName) ?? {};
        if (commandResponse) {
            const dumbFunction = () => commandResponse;
            //this is where i build the strategy thingy i need
            const specialUserConstruct = ({
                long2000Characters,
                args,
                id,
                appendUserMentionToFront,
                replyStrategy,
                userReplyMention
            }) => {
                return replyStrategy({
                    userReplyMention,
                    strategyReplyFunction: dumbFunction,
                    appendStrategy: noAppendStrategy,
                    splitStrategy: long2000Characters
                });
            };
            return specialUserConstruct;
        }
        //no special command found return this dumb shit
        return false;
    } catch (error) {
        throw error;
    } finally {
        dbClient.close();
    }
};

//this is the very last thing where all the mappings come together for these dumb functions

module.exports = {
    getUserResponseStrategy,
    mappings: {
        ...addcommand.mappings,
        ...listcommands.mappings,
        ...listcommandnames.mappings,
        ...deletecommand.mappings
    }
};
