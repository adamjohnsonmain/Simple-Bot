const { helpRegister } = require('../../domain/statics');
const { getClient } = require('../../../utils/mongoTools/openConnection');
const addCommand = async (args) => {
    // eslint-disable-next-line prefer-const
    let [serverId, commandName, ...commandResponse] = args;
    if (!commandName) {
        return 'Please provide a command name as the first argument';
    }
    if (commandName.includes('\n') || commandName.includes('\t')) {
        return 'No new lines or tabs allowed in the command name';
    }
    if (!commandResponse.length) {
        return 'You must provide something for me to response with as the second argument.';
    }

    //normalized the command name
    commandName = commandName.toLowerCase();
    let commandData = null;
    //time to build a db query
    const dbClient = await getClient();
    try {
        //this is where we query!
        //console.log('attempting to connect to commands collection');
        const commandCol = await dbClient.collection('commands');
        const dbFilter = {
            guildId: serverId,
            commandName
        };
        [commandData] = await commandCol
            .find(dbFilter)
            .limit(1)
            .toArray();
        //if command already exists
        if (commandData) {
            //exists already return a message
            return `${commandName} already exists `;
        }
        //doesn't exist so we can go ahead and insert it into the db
        commandData = {
            guildId: serverId,
            commandName,
            commandResponse: commandResponse.join(' ')
        };
        await commandCol.insertOne(commandData);
        //await writeResponses(cachedResponses);
        return `Added your command test it out with \`@diavolo ${commandName}\``;
    } catch (error) {
        throw error;
    } finally {
        dbClient.close();
    }
};

// ---- start setup for addCommand
addCommand.key = 'addCommand';
const addCommandTriggers = ['addcommand'];
helpRegister(
    'addCommand',
    'You can use addcommand <commandname> <reply> to add a stupid message for me to respond with. Commandname cannot contain spaces.'
);
const addCommandConstruct = ({ args, id, appendUserMentionToFront, serverId, replyStrategy, userReplyMention }) => {
    args.unshift(serverId);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: addCommand,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const addCommandMappings = addCommandTriggers.reduce((acc, key) => {
    acc[key] = addCommandConstruct;
    return acc;
}, {});
module.exports = {
    mappings: addCommandMappings
};
