const { ActionRowBuilder, ButtonBuilder, EmbedBuilder } = require('discord.js');
const backId = 'back';
const forwardId = 'forward';
const SECONDARY = 2;
const backButton = new ButtonBuilder({
    style: SECONDARY,
    label: 'Back',
    emoji: '⬅️',
    customId: backId
});
const forwardButton = new ButtonBuilder({
    style: SECONDARY,
    label: 'Forward',
    emoji: '➡️',
    customId: forwardId
});

const createEmbedOptions = async ({ message, commandResults }) => {
    if (commandResults.length === 0) {
        return {
            options: {
                embeds: [],
                meta: {
                    commandResults
                }
            }
        };
    }
    const embedThing = await generateEmbed(0, commandResults);
    return {
        options: {
            embeds: [embedThing],
            meta: {
                commandResults
            }
        }
    };
};

const generateEmbed = (start, commandResults) => {
    const current = commandResults[start];
    // make our embed piece
    return new EmbedBuilder({
        title: 'Custom Commands',
        fields: [
            {
                name: 'Command Name',
                value: `${current.commandName}`
            },
            {
                name: 'Command Response',
                value: `${current.commandResponse}`
            }
        ]
    });
};

const generateEmptyEmbed = () => {
    return new EmbedBuilder({
        title: 'No Commands Found'
    });
};
const embedSendStrategy = async ({ message, content = '', options = {} }) => {
    // since we modify the options we need to extract our items from it
    const {
        embeds: [embed] = [],
        meta: { commandResults = [] }
    } = options;
    // 0 results check just send an message
    if (commandResults?.length === 0) {
        const emptyEmbed = generateEmptyEmbed();
        await message.channel.send({
            embeds: [emptyEmbed]
        });
        return;
    }
    const { author, channel } = message;
    // calculate our actions controllers
    const canFitOnOnePage = commandResults.length <= 1;
    // create the embeds properly and setup the list of action row components.
    const embedMessage = await channel.send({
        embeds: [embed],
        components: [
            ...(canFitOnOnePage
                ? []
                : [
                      new ActionRowBuilder({
                          components: [forwardButton]
                      })
                  ])
        ]
    });
    // the case where we do not need to monitor
    if (canFitOnOnePage) return;

    // Collect button interactions (when a user clicks a button),
    // but only when the button as clicked by the original message author
    const collector = embedMessage.createMessageComponentCollector({
        filter: ({ user }) => user.id === author.id,
        // time limit 15 minute (in ms)
        time: 60000 * 15
    });
    let currentIndex = 0;
    collector.on('collect', async (interaction) => {
        // depending on the action taken we need to perform a function
        switch (interaction.customId) {
            case 'back':
                currentIndex -= 1;
                break;
            case 'forward':
                currentIndex += 1;
                break;
        }
        const currentAdded = commandResults[currentIndex];
        // Increase/decrease index
        // Respond to interaction by updating message with new embed
        await interaction.update({
            embeds: [generateEmbed(currentIndex, commandResults)],
            components: [
                new ActionRowBuilder({
                    components: [
                        // back button if it isn't the start
                        ...(currentIndex ? [backButton] : []),
                        // forward button if it isn't the end
                        ...(currentIndex + 1 < commandResults.length ? [forwardButton] : [])
                    ]
                })
            ]
        });
    });
};
module.exports = { embedSendStrategy, createEmbedOptions };
