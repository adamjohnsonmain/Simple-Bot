/* eslint-disable require-await */
const { helpRegister } = require('../../domain/statics');
const { getClient } = require('../../../utils/mongoTools/openConnection');
const { embedSendStrategy, createEmbedOptions } = require('../embedHelper/embedListedCommands');

const listCommands = async ([serverId, message]) => {
    const dbClient = await getClient();
    try {
        //this is where we query!
        //console.log('attempting to connect to commands collection');
        const commandCol = await dbClient.collection('commands');

        const selector = {
            guildId: serverId
        };
        const response = await commandCol.find(selector).toArray();

        const returnVal = await createEmbedOptions({ message, commandResults: response });

        return returnVal;
    } catch (error) {
        //console.log(error);
        throw error;
    } finally {
        dbClient.close();
    }
};

listCommands.key = 'listCommands';
const listCommandsTriggers = ['listcommand', 'listcommands'];
helpRegister('listcommands', 'You can use listcommands to list all the stupid shit people added to me.');
const listCommandConstruct = ({
    message,
    args,
    id,
    appendUserMentionToFront,
    serverId,
    replyStrategy,
    userReplyMention
}) => {
    args.unshift(message);
    args.unshift(serverId);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: listCommands,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg],
        sendStrategy: embedSendStrategy
    });
};

const listCommandMappings = listCommandsTriggers.reduce((acc, key) => {
    acc[key] = listCommandConstruct;
    return acc;
}, {});
module.exports = {
    mappings: listCommandMappings
};
