const { helpRegister } = require('../../domain/statics');
const { getClient } = require('../../../utils/mongoTools/openConnection');

const listCommandNames = async ([serverId]) => {
    const dbClient = await getClient();
    try {
        //this is where we query!
        //console.log('attempting to connect to commands collection');
        const commandCol = await dbClient.collection('commands');

        const selector = {
            guildId: serverId
        };
        const response = await commandCol.find(selector).toArray();
        const commandNames = response.map(({ commandName }) => commandName);
        //console.log(commandNames);

        return `Heres all the commands I know about: ${commandNames.join(', ')}`;
    } catch (error) {
        throw error;
    } finally {
        dbClient.close();
    }
};
// ---- start setup for the list command names
listCommandNames.key = 'listCommandNames';
const listCommandNamesTriggers = ['listcommandname', 'listcommandnames', 'listnames'];
helpRegister('listcommandnames', 'You can use listcommandnames to list just the names of custom commands.');
const listCommandNamesConstruct = ({
    message,
    args,
    id,
    appendUserMentionToFront,
    serverId,
    replyStrategy,
    userReplyMention
}) => {
    args.unshift(serverId);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: listCommandNames,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};

const listCommandNamesMappings = listCommandNamesTriggers.reduce((acc, key) => {
    acc[key] = listCommandNamesConstruct;
    return acc;
}, {});
module.exports = {
    mappings: listCommandNamesMappings
};
