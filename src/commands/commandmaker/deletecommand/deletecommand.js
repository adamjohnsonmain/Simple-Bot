const { helpRegister } = require('../../domain/statics');
const { getClient } = require('../../../utils/mongoTools/openConnection');

const deleteCommand = async ([serverId, commandName]) => {
    if (!commandName) {
        return 'Please provide a command name to delete as the first argument';
    }
    const dbClient = await getClient();
    try {
        //this is where we query!
        //console.log('attempting to connect to commands collection');
        const commandCol = await dbClient.collection('commands');

        const selector = {
            guildId: serverId,
            commandName
        };
        const response = await commandCol.removeOne(selector);
        // console.log(response);
        return response.result.n === 1
            ? `I have deleted the \`${commandName}\` function.`
            : 'Command failed to be deleted, perhaps it never existed.';
    } catch (error) {
        throw error;
    } finally {
        dbClient.close();
    }
};
// ---- start setup for delete command
deleteCommand.key = 'deletecommand';
const deleteCommandTriggers = ['deletecommand', 'removecommand'];
helpRegister(
    'deletecommand',
    'You can use deletecommand <existingcommandname> to delete that stupid thing you addded to me.'
);
const deleteCommandConstruct = ({ args, id, appendUserMentionToFront, serverId, replyStrategy, userReplyMention }) => {
    args.unshift(serverId);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: deleteCommand,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const deleteCommandMappings = deleteCommandTriggers.reduce((acc, key) => {
    acc[key] = deleteCommandConstruct;
    return acc;
}, {});
module.exports = {
    mappings: deleteCommandMappings
};
