const domain = () =>
    'Nessuno può sfuggire dal destino scelto Rimane solo il risultato che voi sarete distrutti L’eterna cima esiste solo per me Puoi cantare canzoni di tristezza nel mondo senza temp';

const helpMessages = {};

const help = ([command = '']) => {
    if (command !== '') {
        //attempt to find the right command
        const message = helpMessages[command];
        return message ?? thinkAboutWhoIsWorthy();
    }
    return `Ask me about any of these commands: ${Object.keys(helpMessages).join(', ')}`;
};

const helpRegister = (command, helpmessage) => {
    helpMessages[command.toLowerCase()] = helpmessage;
};
helpRegister('help', 'Its a help function.');

const thinkAboutWhoIsWorthy = () => `I do not know that command, valid options:${Object.keys(helpMessages).join(', ')}`;

domain.key = 'Domain';
const domainTriggers = ['king'];
const domainConstruct = ({ args, id, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    if (args[0] && args[0].toLowerCase() === 'crimson') {
        return replyStrategy({
            userReplyMention,
            strategyReplyFunction: domain,
            appendStrategy: appendUserMentionToFront,
            splitStrategy: (msg) => [msg]
        });
    } else {
        return replyStrategy({
            userReplyMention,
            strategyReplyFunction: thinkAboutWhoIsWorthy,
            appendStrategy: appendUserMentionToFront,
            splitStrategy: (msg) => [msg]
        });
    }
};
const domainMappings = domainTriggers.reduce((acc, key) => {
    acc[key] = domainConstruct;
    return acc;
}, {});

help.key = 'Help';
const helpTriggers = ['help', '?'];
const helpConstruct = ({ args, id, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: help,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const helpMappings = helpTriggers.reduce((acc, key) => {
    acc[key] = helpConstruct;
    return acc;
}, {});
thinkAboutWhoIsWorthy.key = 'thinkAboutWhoIsWorthy';
const defaultTriggers = ['default'];
const defaultConstruct = ({ args, id, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    args.unshift('default');
    return domainConstruct({ args, id, appendUserMentionToFront, replyStrategy, userReplyMention });
};
const defaultMappings = defaultTriggers.reduce((acc, key) => {
    acc[key] = defaultConstruct;
    return acc;
}, {});
const mappings = { ...defaultMappings, ...domainMappings, ...helpMappings };
module.exports = {
    domain,
    help,
    helpRegister,
    thinkAboutWhoIsWorthy,
    mappings
};
