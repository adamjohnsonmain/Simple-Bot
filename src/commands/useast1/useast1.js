const useast1 = () => {
    return 'Us east 1 is currently down';
};
const appendStrategy = ({ reply }) => `${reply}`;

useast1.key = 'useast1';
const useast1Triggers = ['useast1'];
const useast1Construct = ({ args, id, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: useast1,
        appendStrategy,
        splitStrategy: (msg) => [msg]
    });
};
const useast1sMappings = useast1Triggers.reduce((acc, key) => {
    acc[key] = useast1Construct;
    return acc;
}, {});

const mappings = { ...useast1sMappings };
module.exports = {
    useast1,
    mappings
};
