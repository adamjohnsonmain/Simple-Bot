/*
    what does a thumbs up record look like in mongo?
    {
        "guildId": xxxx.
        "userId": xxxx,
        "numberOfThumbsUps": 12312312,
        "supporters": [
            {id:xxxx, supportCount: 123},
            ...
        ]
    }
*/
const { getClient } = require('../../utils/mongoTools/openConnection');
const topSupporters = async (args) => {
    const dbClient = await getClient();
    //this is where we query!
    //console.log('attempting to connect to thumbsup collection');
    const thumbsUpCol = await dbClient.collection('thumbsup');
    //console.log('attempting to find the record');

    const [firstMention, serverId] = args;
    if (!firstMention) return 'Mention a user to get praise stats';
    if (!firstMention.match(/<@!?(1|\d{17,19})>/g)) return 'Mention a user to get praise stats';
    const id = firstMention.replace(/(<|@|>|!)/g, '');

    const [result = { supporters: [] }] = await thumbsUpCol
        .find({
            guildId: serverId,
            userId: id
        })
        .limit(1)
        .toArray();
    console.log(result);
    const { supporters } = result;
    if (!supporters.length) return `<@${id}> has no supporters`;

    const messageArr = supporters.map(({ id: supporterId, supportCount }) => {
        return `<@${supporterId}> has supported <@${id}> ${supportCount} times.`;
    });
    return messageArr.join('\n');
};
const triggerWords = ['supporters', 'selfsupporters'];
const supportersReplyConstruct = ({ replyStrategy, userReplyMention, serverId, message, args }) => {
    const [firstMention] = args;
    args.unshift(firstMention, serverId);

    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: topSupporters,
        appendStrategy: ({ reply }) => reply,
        splitStrategy: (msg) => [msg]
    });
};
const selfsupportersReplyConstruct = ({ replyStrategy, userReplyMention, serverId, message, args }) => {
    args.unshift(userReplyMention);
    return supportersReplyConstruct({ replyStrategy, userReplyMention, serverId, message, args });
};

const mappings = {
    supporters: supportersReplyConstruct,
    selfsupporters: selfsupportersReplyConstruct
};
topSupporters.key = 'topSupporters';
module.exports = { topSupporters, mappings };
