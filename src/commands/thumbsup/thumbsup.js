const { getClient } = require('../../utils/mongoTools/openConnection');

/*
    what does a thumbs up record look like in mongo?
    {
        "guildId": xxxx.
        "userId": xxxx,
        "numberOfThumbsUps": 12312312,
        "supporters": [
            {id:xxxx, supportCount: 123},
            ...
        ]
    }
*/
const thumbsup = async (args) => {
    // const thumbsupData = await readThumbsupData();
    const dbClient = await getClient();
    //this is where we query!
    console.log('attempting to connect to thumbsup collection');
    const thumbsUpCol = await dbClient.collection('thumbsup');
    const [message, serverId, ...rest] = args; //assum instead args is just a list of args normally
    const senderId = message.author.id; //stiill get this normally
    let triedToPraiseSelf = false;
    const replies = await rest.reduce(async (acc, maybeId) => {
        const awaitedAcc = await acc;
        if (maybeId.match(/<@!?(1|\d{17,19})>/g)) {
            const userToThumbsUp = maybeId.replace(/(<|@|!|>)/g, '');

            if (senderId === userToThumbsUp) {
                triedToPraiseSelf = true;
                return awaitedAcc;
            }
            //find the userToThumbs up record for the guild
            const [result = {}] = await thumbsUpCol
                .find({
                    guildId: serverId,
                    userId: userToThumbsUp
                })
                .limit(1)
                .toArray();
            const { numberOfThumbsUps = 0, supporters = [] } = result;

            const foundSupport = supporters.find(({ id }) => id === senderId);
            if (foundSupport) {
                foundSupport.supportCount = foundSupport.supportCount + 1;
            } else {
                supporters.push({ id: senderId, supportCount: 1 });
            }
            // thumbsupData[userToThumbsUp] = {
            //     thumbsupCount: thumbsupCount + 1,
            //     supporters
            // };

            //const dataToWrite = JSON.stringify(thumbsupData);
            const filter = {
                guildId: serverId,
                userId: userToThumbsUp
            };
            const upsertRecord = {
                guildId: serverId,
                userId: userToThumbsUp,
                supporters,
                numberOfThumbsUps: numberOfThumbsUps + 1
            };
            await thumbsUpCol.updateOne(filter, upsertRecord, { upsert: true });
            //await writeThumbsUpData(dataToWrite);
            awaitedAcc.push(`<@${userToThumbsUp}> has been praised ${numberOfThumbsUps + 1} times.`);
        }
        return awaitedAcc;
    }, []);
    await dbClient.close();
    if (triedToPraiseSelf) replies.unshift(`<@${senderId}> You cannot thumbs up yourself.`);
    if (!replies.length) return 'Provide a user to praise.';
    return replies.join('\n');
};

thumbsup.key = 'thumbsup';
const triggerWords = ['praise', 'thumbsup', 'bless', 'blessup', 'highfive', '🙏', '👍'];
const constructReplyStrategy = ({ replyStrategy, userReplyMention, message, serverId, args }) => {
    args.unshift(message, serverId);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: thumbsup,
        appendStrategy: ({ reply }) => reply,
        splitStrategy: (msg) => [msg]
    });
};
const mappings = triggerWords.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});
module.exports = {
    thumbsup,
    triggerWords,
    mappings
};
