/*
    what does a thumbs up record look like in mongo?
    {
        "guildId": xxxx.
        "userId": xxxx,
        "numberOfThumbsUps": 12312312,
        "supporters": [
            {id:xxxx, supportCount: 123},
            ...
        ]
    }
*/
const { getClient } = require('../../utils/mongoTools/openConnection');
const numberOfThumbsUp = async (args) => {
    const [firstMention, serverId] = args;
    if (!firstMention.match(/<@!?(1|\d{17,19})>/g)) return 'Mention a user to get praise stats';
    if (!firstMention) return 'Mention a user to get praise stats';
    const id = firstMention.replace(/(<|@|>|!)/g, '');
    const dbClient = await getClient();
    //this is where we query!
    //console.log('attempting to connect to thumbsup collection');
    const thumbsUpCol = await dbClient.collection('thumbsup');
    //console.log('attempting to find the record');

    const [result = { numberOfThumbsUps: 0 }] = await thumbsUpCol
        .find({
            guildId: serverId,
            userId: id
        })
        .limit(1)
        .toArray();
    //console.log('attempting to close');
    await dbClient.close();
    //console.log('attempting to return', result);
    return `<@${id}> has been praised ${result.numberOfThumbsUps} times.`;
};
numberOfThumbsUp.key = 'numberOfThumbsUp';
const triggerWords = ['self'];
const constructReplyStrategy = ({ replyStrategy, userReplyMention, serverId, message, args }) => {
    args.unshift(userReplyMention, serverId);

    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: numberOfThumbsUp,
        appendStrategy: ({ reply }) => reply,
        splitStrategy: (msg) => [msg]
    });
};
const mappings = triggerWords.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});

module.exports = { numberOfThumbsUp, mappings };
