/*
    yoink is dumb, it just looks at the last 5 messages and checks if any have stickers and 
    steals the URLs for you
    console.log();
*/
const { embedSendStrategy, createEmbedOptions } = require('./embedHelper/embedYoinks');

const lookupFile = (format_type) => {
    /*
        PNG	1
        APNG	2
        LOTTIE	3
        GIF	4
    */
    switch (format_type) {
        case 1:
        case 2:
            return '.png';
        case 3:
            return '.lottie';
        case 4:
            return '.gif';
        default:
            return '';
    }
};

const yoink = async (args) => {
    const [message, textchannel] = args;
    const messages = await textchannel.messages.fetch({ limit: 5 });
    const stickerURLs = messages.reduce((acc, msg) => {
        const value = msg?.stickers?.values()?.next()?.value;

        if (value?.id) {
            const fullURL = `https://media.discordapp.net/stickers/${value.id}${lookupFile(value.format)}`;
            acc.push(fullURL);
        }
        return acc;
    }, []);

    // we have an array of stickers we CAN make an embed
    const returnVal = await createEmbedOptions({ message, imageResults: stickerURLs });
    // console.log(returnVal);
    return returnVal;
};
yoink.key = 'yoink';
const yoinkTriggers = ['yoink'];
const yoinkConstruct = ({ args, channel, id, message, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    args.unshift(channel);
    args.unshift(message);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: yoink,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg],
        sendStrategy: embedSendStrategy
    });
};
const yoinkMappings = yoinkTriggers.reduce((acc, key) => {
    acc[key] = yoinkConstruct;
    return acc;
}, {});

const mappings = { ...yoinkMappings };
module.exports = {
    yoink,
    mappings
};
