const { ActionRowBuilder, ButtonBuilder, EmbedBuilder } = require('discord.js');
const backId = 'back';
const forwardId = 'forward';
const SECONDARY = 2;
const backButton = new ButtonBuilder({
    style: SECONDARY,
    label: 'Back',
    emoji: '⬅️',
    customId: backId
});
const forwardButton = new ButtonBuilder({
    style: SECONDARY,
    label: 'Forward',
    emoji: '➡️',
    customId: forwardId
});

const createEmbedOptions = async ({ message, imageResults }) => {
    if (imageResults.length === 0) {
        return {
            options: {
                embeds: [],
                meta: {
                    imageResults
                }
            }
        };
    }
    const embedThing = await generateEmbed(0, imageResults);
    return {
        options: {
            embeds: [embedThing],
            meta: {
                imageResults
            }
        }
    };
};

const generateEmbed = (start, imageResults) => {
    const current = imageResults[start];
    // make our embed piece
    return new EmbedBuilder({
        title: 'Yoinked Sticker',
        fields: [
            {
                name: 'URL',
                value: `<${current}>`,
                inline: true
            }
        ],
        image: {
            url: current
        }
    });
};
const embedSendStrategy = async ({ message, content = '', options = {} }) => {
    // since we modify the options we need to extract our items from it
    const {
        embeds: [embed] = [],
        meta: { imageResults = [] }
    } = options;
    // 0 results check just send an message
    if (imageResults?.length === 0) {
        await message.channel.send({
            content: 'No images found'
        });
        return;
    }
    const { author, channel } = message;
    // calculate our actions controllers
    const canFitOnOnePage = imageResults.length <= 1;
    // create the embeds properly and setup the list of action row components.
    const embedMessage = await channel.send({
        embeds: [embed],
        components: [
            ...(canFitOnOnePage
                ? []
                : [
                      new ActionRowBuilder({
                          components: [forwardButton]
                      })
                  ])
        ]
    });
    // the case where we do not need to monitor
    if (canFitOnOnePage) return;

    // Collect button interactions (when a user clicks a button),
    // but only when the button as clicked by the original message author
    const collector = embedMessage.createMessageComponentCollector({
        filter: ({ user }) => user.id === author.id,
        // time limit 30 seconds (in ms)
        time: 30000
    });
    let currentIndex = 0;
    collector.on('collect', async (interaction) => {
        // depending on the action taken we need to perform a function
        switch (interaction.customId) {
            case 'back':
                currentIndex -= 1;
                break;
            case 'forward':
                currentIndex += 1;
                break;
        }
        const currentAdded = imageResults[currentIndex];
        // Increase/decrease index
        // Respond to interaction by updating message with new embed
        await interaction.update({
            embeds: [generateEmbed(currentIndex, imageResults)],
            components: [
                new ActionRowBuilder({
                    components: [
                        // back button if it isn't the start
                        ...(currentIndex ? [backButton] : []),
                        // forward button if it isn't the end
                        ...(currentIndex + 1 < imageResults.length ? [forwardButton] : [])
                    ]
                })
            ]
        });
    });
};
module.exports = { embedSendStrategy, createEmbedOptions };
