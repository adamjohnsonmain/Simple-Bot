const { helpRegister } = require('../domain/statics');
const charToEmoji = {
    a: '🇦',
    b: '🇧',
    c: '🇨',
    d: '🇩',
    e: '🇪',
    f: '🇫',
    g: '🇬',
    h: '🇭',
    i: '🇮',
    j: '🇯',
    k: '🇰',
    l: '🇱',
    m: '🇲',
    n: '🇳',
    o: '🇴',
    p: '🇵',
    q: '🇶',
    r: '🇷',
    s: '🇸',
    t: '🇹',
    u: '🇺',
    v: '🇻',
    w: '🇼',
    x: '🇽',
    y: '🇾',
    z: '🇿',
    ' ': ' '
};
//what does this comment mean
//no we have the full mappings

const emoji = (args) => {
    if (!args.length) {
        return 'You must pass me a word to turn into emojis';
    }
    const word = args.join(' ').toLowerCase();
    const check = /^[a-z\s]+$/.test(word);
    return check
        ? [...word]
              .map((letter) => {
                  //find new letter
                  const newLetter = charToEmoji[letter];
                  return newLetter;
              })
              .join(' ')
        : 'I only emojify words with letters A-Z';
};

const triggerWords = ['emoji'];
helpRegister(
    'emoji',
    'You can invoke emoji with anyt string containing only letters A-Z to have it converted to regional indicator characters'
);
const constructReplyStrategy = ({ long2000Characters, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    return replyStrategy({
        userReplyMention: '',
        strategyReplyFunction: emoji,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const mappings = triggerWords.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});
module.exports = { emoji, mappings };
