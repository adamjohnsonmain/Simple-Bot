/* eslint-disable no-nested-ternary */
const { helpRegister } = require('../domain/statics');

const emojiToNumber = {
    '💯': '100'
};

const emojiPreprocesing = (value) => {
    return emojiToNumber[value] ? emojiToNumber[value] : value;
};

const dieRoll = (numberToRoll) => {
    if (/[0-9]*d[0-9]+/g.test(numberToRoll)) {
        //dice rolling logic maybe
        const [timesToRoll = '', rolledDie = ''] = numberToRoll.split('d');

        const dieToRoll = emojiPreprocesing(rolledDie);
        if (timesToRoll.length) {
            //cool we have a good situation
            //try to make them both numbers
            const numberOfRolls = Number(timesToRoll);
            const numberDie = Number(dieToRoll);
            if (!Number.isNaN(numberOfRolls) && !Number.isNaN(numberDie)) {
                //real number in here
                const flooredRolls = Math.floor(numberOfRolls);
                const flooredDie = Math.floor(numberDie);
                if (flooredRolls < 1) return 'You must roll at least once.';
                if (flooredDie <= 1) return 'You must at least flip a coin.';
                if (flooredRolls > 10) return 'You cannot roll more than 10 at a time.';
                const rolledValues = [];
                for (let i = 0; i < flooredRolls; i++) {
                    rolledValues.push(Math.ceil(Math.random() * flooredDie));
                }

                return `You rolled: ${rolledValues.join(', ')}\nSum:${rolledValues.reduce((a, b) => a + b, 0)}`;
            }
        }
        //okay maybe dieToRoll is lengthy
        if (dieToRoll.length) {
            //cool we might be able to roll
            const numberDie = Number(dieToRoll);
            if (!Number.isNaN(numberDie)) {
                //real number in here
                const flooredDie = Math.floor(numberDie);
                if (flooredDie <= 1) return 'You must at least flip a coin.';
                const rolledValues = [];
                for (let i = 0; i < 1; i++) {
                    rolledValues.push(Math.ceil(Math.random() * flooredDie));
                }

                return `You rolled: ${rolledValues.join(', ')}\nSum:${rolledValues.reduce((a, b) => a + b, 0)}`;
            }
        }
    }
    return `${numberToRoll} is not a valid roll`;
};

const roll = ([numParam]) => {
    let defaulted = false;
    let numberToRoll = numParam;
    if (!numberToRoll) {
        numberToRoll = 20;
        defaulted = true;
    }
    let rollThis = emojiPreprocesing(numberToRoll);
    rollThis = Number(rollThis);

    if (Number.isNaN(rollThis)) {
        return dieRoll(numberToRoll);
    }
    rollThis = Math.floor(rollThis);
    if (rollThis <= 1) return 'Must roll a postive number greater than 1';

    const rolledVal = Math.ceil(Math.random() * rollThis);
    // add some emoji at the end to indicate bad okay amazing
    const emoji =
        rolledVal === rollThis
            ? ':fire:'
            : rolledVal > rollThis / 2
            ? ':ok_hand:'
            : rolledVal === 1
            ? ':nauseated_face:'
            : ':slight_frown:';
    return `${
        defaulted ? 'Since you provided no number to roll I defaulted to 20. ' : ''
    }You rolled: ${rolledVal} ${emoji}`;
};

roll.key = 'roll';
helpRegister(
    'roll',
    'Its a roll command roll <number>d<number>, defaults to 20 and you can exclude most of the parameter stuff'
);
const triggerWords = ['roll', '🎲'];
const constructReplyStrategy = ({ long2000Characters, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: roll,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: long2000Characters
    });
};
const mappings = triggerWords.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});

module.exports = {
    roll,
    mappings
};
