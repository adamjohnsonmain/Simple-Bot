const { helpRegister } = require('../domain/statics');
const { messages } = require('./8ballData');
const magic8Ball = () => {
    const limit = messages.length;
    return messages[Math.floor(Math.random() * limit)];
};
helpRegister('8ball', 'Call upon the power of the mystic 8ball no parameters required');
magic8Ball.key = 'magic8Ball';
const triggerWords = ['8ball', `🎱`];
const constructReplyStrategy = ({ args, id, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: magic8Ball,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const mappings = triggerWords.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});
module.exports = {
    magic8Ball,
    mappings
};
