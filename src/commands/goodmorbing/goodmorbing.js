const { helpRegister } = require('../domain/statics');
const moment = require('moment');
// the purpose of this file is to uuhhhhhhhhh keep track of the morb day of the week
const bigMorbArray = [
    'https://media.tenor.com/jaOO6q1Qr1cAAAAC/morbius-morbius-sweep.gif',
    'https://media.tenor.com/w_NCV-7DPOwAAAAC/morbius-monday.gif',
    'https://media.tenor.com/n4fkXaAvchIAAAAC/morbius-tuesday.gif',
    'https://media.tenor.com/ZTuavKsTvL4AAAAd/morbius-wednesday-morbius.gif',
    'https://media.tenor.com/XX8M1CxyEFAAAAAd/morbius-thursday-morbius.gif',
    'https://media.tenor.com/7CHrqYVNAGkAAAAd/morbius-morbius-sweep.gif',
    'https://media.tenor.com/x9M3zy_7sqEAAAAd/morbius-saturday-morbius.gif'
];
const goodmorbing = () => {
    // how to code
    /*
        1. figure out the day of the week
        2. 0 MONDAY - 6 SUNDAY 
        3. index into the bigMorbArray
        4. profit
    */
    const day = moment().day();
    const morbDay = bigMorbArray[day];
    return morbDay;
};
const triggerWords = ['goodmorbing', 'goodmorbin', 'morbday', 'morbing'];
helpRegister('goodmorbing', 'returns an appropriate link for the morb day of the week.');
const constructReplyStrategy = ({ long2000Characters, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    return replyStrategy({
        userReplyMention: '',
        strategyReplyFunction: goodmorbing,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const mappings = triggerWords.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});
module.exports = { goodmorbing, mappings };
