const { timeout } = require('../../utils/timeoutHelpers/cancel-timeout');
const { helpRegister } = require('../domain/statics');

const reminders = {
    /*
        formate is:
        id: reminder_promise
    */
};
const s2ms = 1000; //second to ms
const m2ms = 60000; //minute to ms
const h2ms = 3600000; //hour to ms
const d2ms = 86400000; //day to ms
const w2ms = 604800000; //week to ms
const timeLookups = {
    seconds: s2ms,
    second: s2ms,
    sec: s2ms,
    secs: s2ms,
    s: s2ms,
    minutes: m2ms,
    minute: m2ms,
    min: m2ms,
    mins: m2ms,
    m: m2ms,
    hours: h2ms,
    hour: h2ms,
    hr: h2ms,
    hrs: h2ms,
    h: h2ms,
    day: d2ms,
    days: d2ms,
    ds: d2ms,
    d: d2ms,
    week: w2ms,
    weeks: w2ms,
    w: w2ms,
    ws: w2ms
};

const maxDuration = 1814400000; //3 weeks

const reminderPromise = ({ message, id, reminderMessage, timerDuration }) => {
    reminders[id] = timeout(timerDuration);
    reminders[id].promise.then(() => {
        message.channel.send(`${id} Dururururururu: ${reminderMessage}`);
        delete reminders[id];
    });
    return 'I Diavolo will remember this reminder.';
};

const remindme = (args) => {
    //[time, unit, ...junk]
    const [message, id, time, unit, ...reminderMessage] = args;
    if (!id) return 'Something weird happened, no id to associated the remindme with';

    if (reminders[id]) return 'You already have a reminder set Limit 1 at a time.';

    if (!time || !unit) return 'Time and units are required.';
    //attemp to use time number
    const timeNumber = Number(time);
    if (Number.isNaN(timeNumber)) return 'Time parameter must be a number.';
    const unitScale = timeLookups[unit];
    if (!unitScale) return 'Invalid unit provided, week/day/hour/minute/second supported';
    //at this point id, time and unit are defined and proper

    const timerDuration = unitScale * timeNumber;
    if (timerDuration > maxDuration) return 'Sorry I can only remind you 3 weeks out.';

    //Nice we can make our cancelable promise
    return reminderPromise({ message, id, reminderMessage: reminderMessage.join(' '), timerDuration });
};
remindme.key = 'remindme';
const remindMeTriggerWords = ['remindme', 'reminder', 'remember', 'timer', '⏰'];
helpRegister(
    'remindme',
    'Setup a reminder with remindme <number> <seconds/minutes/hours/days> <message>, limit to 1 number and 1 unit and maximum of 3 weeks'
);
const remindmeConstructReplyStrategy = ({
    long2000Characters,
    appendUserMentionToFront,
    replyStrategy,
    userReplyMention,
    args,
    message
}) => {
    args.unshift(message, userReplyMention);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: remindme,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const remindmeMappings = remindMeTriggerWords.reduce((acc, key) => {
    acc[key] = remindmeConstructReplyStrategy;
    return acc;
}, {});

const cancelReminder = (args) => {
    const [id] = args;
    if (reminders[id]) {
        reminders[id].cancel();
        delete reminders[id];
        return 'Deleted your reminder';
    }
    return 'No reminder set, are you mocking me?';
};

cancelReminder.key = 'cancelReminder';
const cancelTriggerWords = ['returntozero', 'cancelthat'];
helpRegister('cancelthat', 'I delete your reminder with cancelthat');

const cancelConstructReplyStrategy = ({ appendUserMentionToFront, replyStrategy, userReplyMention, args }) => {
    args.unshift(userReplyMention);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: cancelReminder,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const cancelMappings = cancelTriggerWords.reduce((acc, key) => {
    acc[key] = cancelConstructReplyStrategy;
    return acc;
}, {});

const mappings = {
    ...remindmeMappings,
    ...cancelMappings
};
module.exports = { remindme, cancelReminder, mappings };
