//this will eventually become a better remind me function.

/*
    General Idea of how this works

    user input -> (number, unit of time, ...message)

    instead of creating a bunch of promises and hoping they get resolved we can use the runners we created maybe

    how will this work

    We will keep an array of reminders
    reminders: [
        {
            userId: unique user ID associated with the reminder
            channel: the channel to send the reminder to
            message: the message associated with the reminder
            activationTime: The timestamp of when to activate
        }
    ]

    adding a reminder aka handling the message
    1. Load up the reminder cache
    2. Create a filtered array for just the userId that we are looking at
    3. Check if the reminder limit has been hit
    4. if it has early exit with max reminders has been hit
    5. Otherwise go the the full array add the reminder and sort by activation time
    6. write new array to disk

    performing the reminders
    Setup: Create a runner on a 1 minute interval
        The function that gets run performs the following
        1. Load in the reminder cache
        2. Check the 0th item in the array for if activation time has come
        3. If acctivation time is not hit early exit and end execution write reamining reminders to disk else continue
        4. Remove 0th element from array and process reminder
        5. Go to step 2 if more elements remain in array
        6. No elements left write empty array to disk
*/
