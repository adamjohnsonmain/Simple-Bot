const { mappings: messageCountMappings } = require('./weekly/messageCounter');
const { mappings: weeklyReportMappings } = require('./report/sevendayreport');
module.exports = {
    mappings: {
        ...messageCountMappings,
        ...weeklyReportMappings
    }
};
