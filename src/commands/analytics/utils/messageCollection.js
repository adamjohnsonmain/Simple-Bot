const moment = require('moment');
/*
    this function (given a textchannel) will go through and get all the messages in the last week 
    you can then use the results to do whatever i guess.

    Included in the array is objects of the form:
    {
        createdTimestamp: 1653418091216,
        id: 'message id string',
        content: 'a string of content'
    },
*/
module.exports = async (textchannel, unit = 'w', unitsOut = 7) => {
    const cutOff = moment()
        .subtract(unitsOut, unit)
        .toDate();
    let before = null;
    let messages = null;
    let oldest = null;
    const items = [];
    do {
        messages = await textchannel.messages.fetch({ limit: 100, ...before });
        messages.filter((m) => m.createdAt > cutOff);
        let newitems = [];
        messages.forEach(({ createdTimestamp, id, content, author }) => {
            //do not include bot messages
            if (!author.bot) newitems.push({ createdTimestamp, id, content });
        });

        newitems.sort((a, b) => a.createdTimestamp > b.createdTimestamp);

        const a = newitems.length;
        newitems = newitems.filter(({ createdTimestamp }) => createdTimestamp > cutOff);
        const b = newitems.length;
        items.push(...newitems);
        oldest = newitems.pop();
        before = {
            before: oldest?.id
        };
        //console.log(messages.size, before.before);
    } while (oldest && oldest.createdTimestamp > cutOff);
    //console.log('all done found', items.length, ' messages');
    return items;
};
