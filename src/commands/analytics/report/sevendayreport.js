/*
    weekly report object:
    {
        guildId: xxx,
        channelId: xxx,
        config: {
            title: xxx,
            author: {
                name: xxx,
                icon_url: (url to an image as a string)
            },
            description: 'Your daily S&P500 message report.',
            thumbnail: {
                url: (url to an image as a string)
            },
            image: {
                url: (url to an image as a string)
            },
footer: {
                text: xxx,
                icon_url: (url to an image as a string)
            }            
        },
        negativeDeltaConfig:{
            //same fields as config
        }
    }
*/
const { generateWeeklyNumbers } = require('./utils/processors');
const { generateWeeklyReportEmbed } = require('./utils/generateEmbed');
const getMessage = require('../utils/messageCollection');
const { getClient } = require('../../../utils/mongoTools/openConnection');
const genWeeklyReport = async (args) => {
    //neat stuff going on here so how can we do this?
    const [guildId, channel] = args;
    //should i store values in the mongodb? ABSOLUTELY YES
    const { id } = channel;

    //i guess these can be stored in the db collect: weeklyreport collection
    const dbClient = await getClient();
    let weeklyReportData = null;
    try {
        //this is where we query!
        // console.log('attempting to connect to weeklyreport collection');
        const commandCol = await dbClient.collection('weeklyreport');
        const dbFilter = {
            guildId,
            channelId: id
        };
        [weeklyReportData] = await commandCol
            .find(dbFilter)
            .limit(1)
            .toArray();
    } catch (e) {
        throw e;
    } finally {
        dbClient.close();
    }
    if (weeklyReportData) {
        //neat we have information here
        //get messages from last 9 days
        //get the counts from the processor file
        //generate the embed
        //return an object with the embed property
        const msgs = await getMessage(channel, 10, 'd');
        const { yesterday, yesterdayDelta, weekly, weeklyDelta } = generateWeeklyNumbers(msgs);
        const embed = generateWeeklyReportEmbed(weeklyReportData, yesterday, yesterdayDelta, weekly, weeklyDelta);
        return {
            options: {
                embeds: [embed]
            },
            reply: 'The Daily Report'
        };
    } else {
        return 'No reporting config is setup for this channel, reach out to the admin to set this up';
    }
};
const triggerWords = ['dailyreport'];
// helpRegister(
//     'messagecount',
//     'Tells you how many messages were sent in a channel. Usage `messagecount optional_value optional_unit` the values default to 1 week'
// );
const constructReplyStrategy = ({
    long2000Characters,
    channel,
    appendUserMentionToFront,
    replyStrategy,
    serverId,
    userReplyMention,
    args
}) => {
    //need to unshift the textchannel and guildId
    args.unshift(serverId, channel);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: genWeeklyReport,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: long2000Characters
    });
};
const mappings = triggerWords.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});

module.exports = {
    genWeeklyReport,
    mappings
};
