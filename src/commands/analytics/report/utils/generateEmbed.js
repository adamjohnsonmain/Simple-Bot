const generateTrend = (deltaString) => {
    const number = Number(deltaString);
    if (number > 0) {
        return ' 📈';
    } else if (number < 0) {
        return ' 📉';
    } else {
        return '';
    }
};
const selectConfig = (config, negativeDeltaConfig, yesterdayDelta) => {
    //if a negativeDeltaConfig is defined then use it only if yesterday was negative
    if (negativeDeltaConfig && Number(yesterdayDelta) < 0) {
        return negativeDeltaConfig;
    }
    //otherwise just return the normal config
    return config;
};
const generateWeeklyReportEmbed = (weeklyReportData, yesterday, yesterdayDelta, weekly, weeklyDelta) => {
    const { config, negativeDeltaConfig } = weeklyReportData;
    //config has PRETTY MUCH EVERYTHING
    const embed = {
        ...selectConfig(config, negativeDeltaConfig, yesterdayDelta),
        fields: [
            {
                name: 'Todays Date',
                value: new Date().toDateString()
            },
            {
                name: 'Yesterday',
                value: `${yesterday} (${yesterdayDelta})${generateTrend(yesterdayDelta)}`,
                inline: true
            },
            {
                name: '7 day average',
                value: `${weekly} (${weeklyDelta})${generateTrend(weeklyDelta)}`,
                inline: true
            }
        ],
        timestamp: new Date()
    };
    return embed;
};

module.exports = {
    generateWeeklyReportEmbed
};
