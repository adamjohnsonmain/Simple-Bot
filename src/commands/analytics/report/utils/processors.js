/*
    im too dumb to name this file right so basically
    there is 1 function that takes an input array of messages
    with a createdTimestamp in each object and generates numbers
    
*/
const moment = require('moment');
const generateDeltaBetweenTwoNumbers = (a, b) => {
    /*
        we are comparing a to b
        a - b > 0 positive change
        a - b = 0 no change
        a - b < 0 negative change
    */
    const delta = a - b;
    return `${delta > 0 ? '+' : ''}${delta}`;
};
/*
    this function assums msgs are from 9 days or older
*/

const generateWeeklyNumbers = (msgs) => {
    //-1 day
    const yesterdayBin = [];
    //-2 day
    const dayBeforeYesterdayBin = [];
    //-7 through -1
    const weeklyBin = [];
    //-8 through -2
    const yesterWeeklyBin = [];
    /*
        generate 3 dates: (ASSUME EST)
        Today @ 12AM end
        Yesterday @ 12AM yesterdayStart
        1 week ago @ 12AM weeklyStart
        if msg.createdTimestamp > end -> do nothing
        if msg.createdTimestamp >= yesterdayStart -> push into yesterdayBin && weekly bin
        if msg.createdTimestamp < yesterdayStart && msg.createdTimestamp >= dayBeforeYesterdayStart -> push into yesterdayBin && weekly bin
        if msg.createdTimestamp < dayBeforeYesterdayStart && msg.createdTimestamp >= weeklyStart -> push into weeklyBin 

        taking into account yesterweekly
        if createdTimestamp > end do nothing
        if createdTimestamp >= yesterdayStart
            yesterdayBin
            weeklyBin
        if createdTimestamp <=

        else -> do nothing
    */
    const end = moment()
        .local()
        .startOf('day')
        .toDate();
    const yesterdayStart = moment()
        .subtract(1, 'd')
        .local()
        .startOf('day')
        .toDate();
    const dayBeforeYesterdayStart = moment()
        .subtract(2, 'd')
        .local()
        .startOf('day')
        .toDate();
    const weeklyStart = moment()
        .subtract(1, 'w')
        .local()
        .startOf('day')
        .toDate();
    const yesterWeeklyStart = moment()
        .subtract(8, 'd')
        .local()
        .startOf('day')
        .toDate();
    msgs.forEach((msg) => {
        const { createdTimestamp } = msg;
        if (createdTimestamp >= end) {
            // eslint-disable-next-line no-useless-return
            return;
        } else if (createdTimestamp >= yesterdayStart) {
            //first check adds to both yesterday and weekly
            yesterdayBin.push(msg);
            weeklyBin.push(msg);
        } else if (createdTimestamp >= dayBeforeYesterdayStart) {
            //second check adds to both dayBeforeYesterday and weekly and yesterWeekly
            dayBeforeYesterdayBin.push(msg);
            weeklyBin.push(msg);
            yesterWeeklyBin.push(msg);
        } else if (createdTimestamp >= weeklyStart) {
            //third check puts items into weekly and yesterweekly
            weeklyBin.push(msg);
            yesterWeeklyBin.push(msg);
        } else if (createdTimestamp >= yesterWeeklyStart) {
            //fourth check finishes up messages for yesterweekly
            yesterWeeklyBin.push(msg);
        }
    });
    const yesterdayDelta = generateDeltaBetweenTwoNumbers(yesterdayBin.length, dayBeforeYesterdayBin.length);
    const weeklyDelta = generateDeltaBetweenTwoNumbers(
        Math.round(weeklyBin.length / 7),
        Math.round(yesterWeeklyBin.length / 7)
    );
    return {
        yesterday: yesterdayBin.length,
        yesterdayDelta,
        weekly: Math.round(weeklyBin.length / 7), //:5head: yea i do averages
        weeklyDelta
    };
};
module.exports = {
    generateWeeklyNumbers
};
