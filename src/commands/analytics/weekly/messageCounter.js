//cool what does this function do
const dateUnits = require('../utils/dateUnits.json');
const dateUnitsToDays = require('../utils/dateUnitToDays.json');
const getMessages = require('../utils/messageCollection');
const { helpRegister } = require('../../domain/statics');

const messageCounter = async (args) => {
    //args should be setup with channel, value, unit
    const [textchannel, value = 1, unit = 'w'] = args;
    /*
        textchannel is a given since the command can only be called in a channel (ihope)
        value should be an integer > 0 
    */
    if (!Number.isInteger(Number(value)) || Number(value) <= 0) {
        return 'Your value is not an integer > 0';
    }
    const trueUnit = dateUnits[unit];
    if (!trueUnit) {
        return `${unit} is not a recognized value please use values from this list: ${Object.keys(dateUnits).join(
            ', '
        )}`;
    }
    //only allow for 2 weeks back at most
    const unitInDays = dateUnitsToDays[trueUnit];
    if (unitInDays * value > 14) {
        return 'You are restricted to 14 days back at most';
    }
    //we have everything we need here to get a list of all messages
    const messageList = await getMessages(textchannel, trueUnit, Number(value));
    return `There have been ${messageList.length} messages sent in the last ${unitInDays * value}day(s)`;
};
const triggerWords = ['messagecount'];
helpRegister(
    'messagecount',
    'Tells you how many messages were sent in a channel. Usage `messagecount optional_value optional_unit` the values default to 1 week'
);
const constructReplyStrategy = ({
    long2000Characters,
    channel,
    appendUserMentionToFront,
    replyStrategy,
    userReplyMention,
    args
}) => {
    //need to unshift the textchannel
    args.unshift(channel);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: messageCounter,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: long2000Characters
    });
};
const mappings = triggerWords.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});

module.exports = {
    messageCounter,
    mappings
};
