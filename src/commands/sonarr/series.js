const { addSeries } = require('../../utils/sonarrAPI/apiWrapper');
const { helpRegister } = require('../domain/statics');

//THIS IS REALLY STUPID BUT IM DUMB SO HERE IT IS
const {
    sonarr: { approvedChannels }
} = require('../../../config.json');
const addSeriesHandler = async (args) => {
    const [channel, id] = args;

    if (!approvedChannels.includes(channel.id)) {
        return 'Sorry this is the wrong channel for this command';
    }
    //i forget if args has all query terms i think args is just everything after the command and we need to join it
    try {
        const results = await addSeries(id);
        return results.message;
    } catch (e) {
        return 'Sorry there was an error';
    }
};
addSeriesHandler.key = 'addseries';
const addseriesTriggers = ['addseries'];
const addseriesConstruct = ({ args, id, appendUserMentionToFront, replyStrategy, userReplyMention, channel }) => {
    args.unshift(channel);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: addSeriesHandler,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg]
    });
};
const downloadMappings = addseriesTriggers.reduce((acc, key) => {
    acc[key] = addseriesConstruct;
    return acc;
}, {});
helpRegister(
    'addseries',
    'usage `addseries id` id is the tvdb of the show you want to attempt to add and fetch episodes for. Only anime will work'
);
const mappings = { ...downloadMappings };
module.exports = {
    addSeriesHandler,
    mappings
};
