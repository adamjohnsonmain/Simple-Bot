const { lookupSeries } = require('../../utils/sonarrAPI/apiWrapper');
//THIS IS REALLY STUPID BUT IM DUMB SO HERE IT IS
const {
    sonarr: { approvedChannels }
} = require('../../../config.json');
const { helpRegister } = require('../domain/statics');
const { createEmbedOptions, embedSendStrategy } = require('./embedHelper/embedLookup');
const lookup = async (args) => {
    const [message, channel, ...rest] = args;

    if (approvedChannels.length && !approvedChannels.includes(channel.id)) {
        return { reply: 'Sorry this is the wrong channel for this command' };
    }
    //i forget if args has all query terms i think args is just everything after the command and we need to join it
    const term = rest.join(' ');
    try {
        const results = await lookupSeries(term);
        const returnVal = await createEmbedOptions({ message, lookupResults: results });
        // console.log(returnVal);
        return returnVal;
    } catch (e) {
        return { reply: 'Sorry there was an error' };
    }
};
helpRegister(
    'lookup',
    'usage `lookup <terms>` terms are just space separated words which will use sonarr to query tvdb for shows'
);
lookup.key = 'lookup';
const lookupTriggers = ['lookup'];
const lookupConstruct = ({ args, id, appendUserMentionToFront, replyStrategy, userReplyMention, channel, message }) => {
    args.unshift(channel);
    args.unshift(message);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: lookup,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: (msg) => [msg],
        sendStrategy: embedSendStrategy
    });
};
const downloadMappings = lookupTriggers.reduce((acc, key) => {
    acc[key] = lookupConstruct;
    return acc;
}, {});

const mappings = { ...downloadMappings };
module.exports = {
    lookup,
    mappings
};
