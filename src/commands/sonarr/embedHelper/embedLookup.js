const { ActionRowBuilder, ButtonBuilder, EmbedBuilder } = require('discord.js');
const { addSeries } = require('../../../utils/sonarrAPI/apiWrapper');
const backId = 'back';
const forwardId = 'forward';
const addId = 'add';
const SECONDARY = 2;
const backButton = new ButtonBuilder({
    style: SECONDARY,
    label: 'Back',
    emoji: '⬅️',
    customId: backId
});
const forwardButton = new ButtonBuilder({
    style: SECONDARY,
    label: 'Forward',
    emoji: '➡️',
    customId: forwardId
});
const addSeriesButton = new ButtonBuilder({
    style: SECONDARY,
    label: 'Add Series',
    emoji: '✅',
    customId: addId
});

const createEmbedOptions = async ({ message, lookupResults }) => {
    // console.log('lookup results');
    if (lookupResults.length === 0) {
        return {
            options: {
                embeds: [],
                meta: {
                    lookupResults
                }
            }
        };
    }
    const embedThing = await generateEmbed(0, message, lookupResults);
    return {
        options: {
            embeds: [embedThing],
            meta: {
                lookupResults
            }
        }
    };
};

const embedSendStrategy = async ({ message, content = '', options = {} }) => {
    // since we modify the options we need to extract our items from it
    const { embeds: [embed] = [], meta: { lookupResults = [] } = {} } = options;
    // 0 results check just send an message
    if (lookupResults?.length === 0) {
        await message.channel.send({
            content: content || `This channel might be unable to use the lookup command, or an error occured`
        });
        return;
    }
    const { author, channel } = message;
    // calculate our actions controllers
    const canFitOnOnePage = lookupResults.length <= 1;
    const firstAdded = lookupResults[0]?.seriesAdded;
    // create the embeds properly and setup the list of action row components.
    const embedMessage = await channel.send({
        embeds: [embed],
        components: [
            new ActionRowBuilder({
                components: [...(firstAdded ? [] : [addSeriesButton]), ...(canFitOnOnePage ? [] : [forwardButton])]
            })
        ]
    });
    // the case where we do not need to monitor
    if (canFitOnOnePage && firstAdded) return;

    // Collect button interactions (when a user clicks a button),
    // but only when the button as clicked by the original message author
    const collector = embedMessage.createMessageComponentCollector({
        filter: ({ user }) => user.id === author.id,
        // time limit 30 seconds (in ms)
        time: 30000
    });
    let currentIndex = 0;
    collector.on('collect', async (interaction) => {
        // depending on the action taken we need to perform a function
        switch (interaction.customId) {
            case 'back':
                currentIndex -= 1;
                break;
            case 'forward':
                currentIndex += 1;
                break;
            case 'add':
                // add goes ahead and calls our helper add series function then alerts the user the series was added
                // afterwards we fall through the regen the embed
                try {
                    await addSeries(lookupResults[currentIndex]?.tvdbId);
                    lookupResults[currentIndex].seriesAdded = true;
                    await message.channel.send({ content: `Series has been added` });
                } catch (e) {
                    await message.channel.send({ content: 'Failed to add try again later' });
                }
        }
        const currentAdded = lookupResults[currentIndex].seriesAdded;
        // Increase/decrease index
        // Respond to interaction by updating message with new embed
        await interaction.update({
            embeds: [generateEmbed(currentIndex, message, lookupResults)],
            components: [
                new ActionRowBuilder({
                    components: [
                        // add series button if the series isn't added
                        ...(currentAdded ? [] : [addSeriesButton]),
                        // back button if it isn't the start
                        ...(currentIndex ? [backButton] : []),
                        // forward button if it isn't the end
                        ...(currentIndex + 1 < lookupResults.length ? [forwardButton] : [])
                    ]
                })
            ]
        });
    });
};

const generateEmbed = (start, message, lookupResults) => {
    const current = lookupResults[start];
    // make our embed piece
    return new EmbedBuilder({
        title: current.title,
        fields: [
            {
                name: 'TVDBID',
                value: `${current.tvdbId}`,
                inline: true
            }
        ],
        image: {
            url: current.remotePoster
        },
        description: current.overview
    });
};

module.exports = { embedSendStrategy, createEmbedOptions };
