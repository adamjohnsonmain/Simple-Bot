const { helpRegister } = require('../domain/statics');
const { fetchAllUnitData, cachedName, cachedExpiry } = require('./utils/fehWikiaFetches');
const { embedSendStrategy, createEmbedOptions } = require('./utils/embedFehStats');
const { mappingHandler } = require('./utils/mappingHandler');
const CacheManager = require('../../utils/cacheManager/inMemoryCacheManager');
const cacheManager = new CacheManager();
const randomUnit = async (args) => {
    //we need the cache manager
    const [message] = args;
    // step 1 is trying to remember how the cache manager works and seeing if our BIG
    // LIST OF DATA is cached
    let fehData = {};
    try {
        const { item, expired } = cacheManager.getCachedItem(cachedName);
        if (expired) {
            console.log('feh data expired making a refresh');
            // we need to make the request :)
            fehData = await fetchAllUnitData();
            cacheManager.registerCacheItem(cachedName, fehData, { expiresIn: cachedExpiry });
        } else {
            // the data is our item
            fehData = item;
        }
    } catch (e) {
        throw new Error('things went horribly wrong im sorry');
    }
    /*
        {
            title: {
                WikiName: 'Lyn Blazing Whirlwind',
                page: 'Lyn: Blazing Whirlwind',
                name: 'Lyn: Blazing Whirlwind',
                Entries: 'Fire Emblem: The Blazing Blade',
                MoveType: 'Infantry',
                WeaponType: 'Blue Lance',
                Lv1HP5: '17',
                Lv1Atk5: '8',
                Lv1Spd5: '14',
                Lv1Def5: '8',
                Lv1Res5: '8',
                HPGR3: '50',
                AtkGR3: '70',
                SpdGR3: '70',
                DefGR3: '60',
                ResGR3: '55',
                ReleaseDate: '2022-09-07',
                'Properties  full': 'special,hat,tiara',
                ReleaseDate__precision: '1'
            }
        }
    */
    const randomHero = fehData[Math.floor(Math.random() * fehData.length)];
    const { enrichedData } = await mappingHandler([randomHero]);
    const embedMessage = await createEmbedOptions({ message, preppedEntries: enrichedData });
    return embedMessage;
};
randomUnit.key = 'hero';
const randomUnitTriggers = ['randomunit'];
helpRegister('unitinfo', 'Ask me for info on any fire emblem character :)');
const constructReplyStrategy = ({
    long2000Characters,
    appendUserMentionToFront,
    replyStrategy,
    userReplyMention,
    message,
    args
}) => {
    args.unshift(message);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: randomUnit,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: long2000Characters,
        sendStrategy: embedSendStrategy
    });
};
const mappings = randomUnitTriggers.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});

module.exports = {
    randomUnitTriggers,
    mappings
};
