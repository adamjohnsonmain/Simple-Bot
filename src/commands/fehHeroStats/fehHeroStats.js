const { helpRegister } = require('../domain/statics');
const { fetchAllUnitData, cachedName, cachedExpiry } = require('./utils/fehWikiaFetches');
const { embedSendStrategy, createEmbedOptions } = require('./utils/embedFehStats');
const { mappingHandler } = require('./utils/mappingHandler');
const { paramExtract } = require('./utils/paramTools');
const CacheManager = require('../../utils/cacheManager/inMemoryCacheManager');
const cacheManager = new CacheManager();
/*

    Gonna save you some time future me looking at this you can do stats by doing
    atk///>30 
    atk///sort|asc
    shit like that
        paramExtract has the code
*/
const unitInformation = async (args) => {
    //we need the cache manager
    const [message, ...rest] = args;
    if (rest.length === 0) {
        const res = await createEmbedOptions({ message, preppedEntries: [] });
        res.reply = 'You must provide arguments';
        return res;
    }
    // step 1 is trying to remember how the cache manager works and seeing if our BIG
    // LIST OF DATA is cached
    let fehData = {};
    try {
        const { item, expired } = cacheManager.getCachedItem(cachedName);
        if (expired) {
            //console.log('feh data expired making a refresh');
            // we need to make the request :)
            fehData = await fetchAllUnitData();
            cacheManager.registerCacheItem(cachedName, fehData, { expiresIn: cachedExpiry });
        } else {
            //console.log('feh data cache used');
            // the data is our item
            fehData = item;
        }
    } catch (e) {
        throw e;
    }
    const { terms, params, sort } = paramExtract(rest);
    const filterWord = terms.join(' ');
    /*
        {
            title: {
                WikiName: 'Lyn Blazing Whirlwind',
                page: 'Lyn: Blazing Whirlwind',
                name: 'Lyn: Blazing Whirlwind',
                Entries: 'Fire Emblem: The Blazing Blade',
                MoveType: 'Infantry',
                WeaponType: 'Blue Lance',
                Lv1HP5: '17',
                Lv1Atk5: '8',
                Lv1Spd5: '14',
                Lv1Def5: '8',
                Lv1Res5: '8',
                HPGR3: '50',
                AtkGR3: '70',
                SpdGR3: '70',
                DefGR3: '60',
                ResGR3: '55',
                ReleaseDate: '2022-09-07',
                'Properties  full': 'special,hat,tiara',
                ReleaseDate__precision: '1'
            }
        }
    */
    const filteredHeroes = fehData.filter(({ title: { WikiName, page, name } }) => {
        return WikiName.includes(filterWord) || page.includes(filterWord) || name.includes(filterWord);
    });
    if (filteredHeroes.length === 0) {
        const res = await createEmbedOptions({ message, preppedEntries: [] });
        res.reply = 'No results found by search tems';
        return res;
    }

    const { enrichedData, zeroResults } = await mappingHandler(filteredHeroes, params, sort);
    if (enrichedData) {
        const embedMessage = await createEmbedOptions({ message, preppedEntries: enrichedData });
        return embedMessage;
    } else if (zeroResults) {
        const res = await createEmbedOptions({ message, preppedEntries: [] });
        res.reply = 'No results found within parameters';
        return res;
    } else {
        const res = await createEmbedOptions({ message, preppedEntries: [] });
        res.reply = 'Too many results try better search terms';
        return res;
    }
};
unitInformation.key = 'unitinfo';
const unitInfoTriggers = ['unitinfo', '📝'];
helpRegister('unitinfo', 'Ask me for info on any fire emblem character :)');
const constructReplyStrategy = ({
    long2000Characters,
    appendUserMentionToFront,
    replyStrategy,
    userReplyMention,
    message,
    args
}) => {
    args.unshift(message);
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: unitInformation,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: long2000Characters,
        sendStrategy: embedSendStrategy
    });
};
const mappings = unitInfoTriggers.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});

module.exports = {
    unitInformation,
    mappings
};
