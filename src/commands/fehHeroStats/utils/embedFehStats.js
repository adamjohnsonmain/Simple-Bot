const { ActionRowBuilder, ButtonBuilder, EmbedBuilder } = require('discord.js');
const backId = 'back';
const forwardId = 'forward';
const SECONDARY = 2;
const backButton = new ButtonBuilder({
    style: SECONDARY,
    label: 'Back',
    emoji: '⬅️',
    customId: backId
});
const forwardButton = new ButtonBuilder({
    style: SECONDARY,
    label: 'Forward',
    emoji: '➡️',
    customId: forwardId
});

const createEmbedOptions = async ({ message, preppedEntries }) => {
    if (preppedEntries.length === 0) {
        return {
            options: {
                embeds: [],
                meta: {
                    preppedEntries
                }
            }
        };
    }
    const embedThing = await generateEmbed(0, preppedEntries);
    return {
        options: {
            embeds: [embedThing],
            meta: {
                preppedEntries
            }
        }
    };
};

const generateEmbed = (start, preppedEntries) => {
    /*
    return {
        mHP,
        mATK,
        mDEF,
        mRES,
        mSPD,
        total,
        entry: {
            WikiName: 'Lyn Blazing Whirlwind',
            page: 'Lyn: Blazing Whirlwind',
            name: 'Lyn: Blazing Whirlwind',
            Entries: 'Fire Emblem: The Blazing Blade',
            MoveType: 'Infantry',
            WeaponType: 'Blue Lance',
            Lv1HP5: '17',
            Lv1Atk5: '8',
            Lv1Spd5: '14',
            Lv1Def5: '8',
            Lv1Res5: '8',
            HPGR3: '50',
            AtkGR3: '70',
            SpdGR3: '70',
            DefGR3: '60',
            ResGR3: '55',
            ReleaseDate: '2022-09-07',
            'Properties  full': 'special,hat,tiara',
            ReleaseDate__precision: '1'
        }
    };
    */
    const current = preppedEntries[start];
    // make our embed piece
    return new EmbedBuilder({
        title: current.entry.page,
        fields: [
            {
                name: 'Movement',
                value: `${current.entry?.MoveType}`
            },
            {
                name: 'Weapon Type',
                value: `${current.entry?.WeaponType}`
            },
            // breaker field
            { name: '\u200B', value: '\u200B' },
            // unit stats embed
            {
                name: 'Health',
                value: `${current.mHP}`,
                inline: true
            },
            {
                name: 'Attack',
                value: `${current.mATK}`,
                inline: true
            },

            {
                name: 'Speed',
                value: `${current.mSPD}`,
                inline: true
            },
            {
                name: 'Defense',
                value: `${current.mDEF}`,
                inline: true
            },
            {
                name: 'Resistance',
                value: `${current.mRES}`,
                inline: true
            },
            {
                name: 'Total BST',
                value: `${current.total}`,
                inline: true
            }
        ],
        image: {
            url: current.image
        }
    });
};
const embedSendStrategy = async ({ message, content = '', options = {} }) => {
    // since we modify the options we need to extract our items from it
    const {
        embeds: [embed] = [],
        meta: { preppedEntries = [] }
    } = options;
    // 0 results check just send an message
    if (preppedEntries?.length === 0) {
        // console.log(JSON.stringify(content));
        await message.channel.send({ content });
        return;
    }
    const { author, channel } = message;
    // calculate our actions controllers
    const canFitOnOnePage = preppedEntries.length <= 1;
    // create the embeds properly and setup the list of action row components.
    const embedMessage = await channel.send({
        embeds: [embed],
        components: [
            ...(canFitOnOnePage
                ? []
                : [
                      new ActionRowBuilder({
                          components: [forwardButton]
                      })
                  ])
        ]
    });
    // the case where we do not need to monitor
    if (canFitOnOnePage) return;

    // Collect button interactions (when a user clicks a button),
    // but only when the button as clicked by the original message author
    const collector = embedMessage.createMessageComponentCollector({
        filter: ({ user }) => user.id === author.id,
        // time limit 15 minute (in ms)
        time: 60000 * 15
    });
    let currentIndex = 0;
    collector.on('collect', async (interaction) => {
        // depending on the action taken we need to perform a function
        switch (interaction.customId) {
            case 'back':
                currentIndex -= 1;
                break;
            case 'forward':
                currentIndex += 1;
                break;
        }
        const currentAdded = preppedEntries[currentIndex];
        // Increase/decrease index
        // Respond to interaction by updating message with new embed
        await interaction.update({
            embeds: [generateEmbed(currentIndex, preppedEntries)],
            components: [
                new ActionRowBuilder({
                    components: [
                        // back button if it isn't the start
                        ...(currentIndex ? [backButton] : []),
                        // forward button if it isn't the end
                        ...(currentIndex + 1 < preppedEntries.length ? [forwardButton] : [])
                    ]
                })
            ]
        });
    });
};
module.exports = { embedSendStrategy, createEmbedOptions };
