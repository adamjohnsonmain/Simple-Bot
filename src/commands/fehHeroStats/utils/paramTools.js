/*
    // true field names
    mHP,mATK,mDEF,mRES,mSPD,total

    // should not be case sensitive
    mHP
        hp
        health
        hitpoints
    mATK
        atk
        attack
    mSPD
        spd
        speed
    mDEF
        def
        defence
        defense
    mRES
        res
        resistance
    total
        total
        bst
    
*/

const validOps = ['>', '<'];
const validSortDirections = ['asc', 'dsc'];

const TRUE_HP_FIELD = 'mHP';
const TRUE_ATK_FIELD = 'mATK';
const TRUE_SPD_FIELD = 'mSPD';
const TRUE_DEF_FIELD = 'mDEF';
const TRUE_RES_FRIELD = 'mRES';
const TRUE_TOTAL_FIELD = 'total';

const fieldMappings = {
    hp: TRUE_HP_FIELD,
    health: TRUE_HP_FIELD,
    hitpoints: TRUE_HP_FIELD,
    atk: TRUE_ATK_FIELD,
    attack: TRUE_ATK_FIELD,
    spd: TRUE_SPD_FIELD,
    speed: TRUE_SPD_FIELD,
    def: TRUE_DEF_FIELD,
    defence: TRUE_DEF_FIELD,
    defense: TRUE_DEF_FIELD,
    res: TRUE_RES_FRIELD,
    resistance: TRUE_RES_FRIELD,
    total: TRUE_TOTAL_FIELD,
    bst: TRUE_TOTAL_FIELD
};

const validFields = Object.keys(fieldMappings);

const paramExtract = (entry) => {
    const result = entry.reduce(
        (acc, word) => {
            if (word.includes('///')) {
                const [field, opVal] = word.split('///');
                if (field && validFields.includes(field.toLowerCase()) && opVal && opVal.length > 1) {
                    const op = opVal[0];
                    if (op.toLowerCase() === 's') {
                        //special sort case {field}///sort|asc
                        const [, sortDirection] = opVal.split('|');
                        if (sortDirection && validSortDirections.includes(sortDirection)) {
                            acc.sort = {
                                field,
                                sortDirection
                            };
                        }
                    } else {
                        const [, val] = opVal.split(op);
                        if (validOps.includes(op) && val && Number.isInteger(Number(val)) && Number(val) > 0) {
                            acc.params.push({
                                field: field.toLowerCase(),
                                op,
                                val: Number(val)
                            });
                        }
                    }
                }
            } else {
                acc.terms.push(word);
            }
            return acc;
        },
        { terms: [], params: [], sort: {} }
    );
    return result;
};
module.exports = {
    paramExtract,
    fieldMappings
};
