/*
entry:
    WikiName	"Lyn Blazing Whirlwind"
    page	"Lyn: Blazing Whirlwind"
    name	"Lyn: Blazing Whirlwind"
    Entries	"Fire Emblem: The Blazing Blade"
    MoveType	"Infantry"
    WeaponType	"Blue Lance"
    Lv1HP5	"17"
    Lv1Atk5	"8"
    Lv1Spd5	"14"
    Lv1Def5	"8"
    Lv1Res5	"8"
    HPGR3	"50"
    AtkGR3	"70"
    SpdGR3	"70"
    DefGR3	"60"
    ResGR3	"55"
    ReleaseDate	"2022-09-07"
    Properties full	"special,hat,tiara"
    ReleaseDate__precision	"1"
*/
/*
    Massive shoutout to the creators of these modules:
    https://feheroes.fandom.com/wiki/Module:FEHStatUtil
    https://feheroes.fandom.com/wiki/Module:MaxStatsTable

*/
const calculateMaxStats = (entry, rarity = 5) => {
    //
    const { Lv1HP5, Lv1Atk5, Lv1Spd5, Lv1Def5, Lv1Res5, HPGR3, AtkGR3, SpdGR3, DefGR3, ResGR3 } = sanitize(entry);
    const mHP = getMaxStat(rarity, Lv1HP5, HPGR3);
    const mATK = getMaxStat(rarity, Lv1Atk5, AtkGR3);
    const mSPD = getMaxStat(rarity, Lv1Spd5, SpdGR3);
    const mDEF = getMaxStat(rarity, Lv1Def5, DefGR3);
    const mRES = getMaxStat(rarity, Lv1Res5, ResGR3);
    const total = mHP + mATK + mSPD + mDEF + mRES;
    return {
        mHP,
        mATK,
        mDEF,
        mRES,
        mSPD,
        total,
        entry
    };
};
const sanitize = ({ Lv1HP5, Lv1Atk5, Lv1Spd5, Lv1Def5, Lv1Res5, HPGR3, AtkGR3, SpdGR3, DefGR3, ResGR3 }) => {
    const obj = { Lv1HP5, Lv1Atk5, Lv1Spd5, Lv1Def5, Lv1Res5, HPGR3, AtkGR3, SpdGR3, DefGR3, ResGR3 };
    return Object.keys(obj).reduce((acc, key) => {
        acc[key] = Number(obj[key]);
        return acc;
    }, {});
};
const getMasterGrowthRate = (rarity, rate) => {
    return Math.floor(rate * (0.79 + 0.07 * rarity));
};

const getGrowthValue = (rarity, rate) => {
    return Math.floor(0.39 * getMasterGrowthRate(rarity, rate));
};

const getMaxStat = (rarity, base, growth) => {
    return base + getGrowthValue(rarity, growth);
};

const getMaxStatUnrounded = (rarity, base, growth) => {
    return base + 0.39 * growth * (0.79 + rarity * 0.07);
};

module.exports = { calculateMaxStats };
