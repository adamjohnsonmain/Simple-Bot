const { getUnitImageWithCaching } = require('./fehWikiaFetches');
const { calculateMaxStats } = require('./statCalcUtils');
const { fieldMappings } = require('./paramTools');

const advancedFilter = (preppedData, params) => {
    if (params.length) {
        const filteredPreppedData = params.reduce((acc, param) => {
            const { field, op, val } = param;
            return acc.filter((unitInfo) => {
                const fieldValue = unitInfo[fieldMappings[field]];
                if (op === '>') {
                    return fieldValue > val;
                } else if (op === '<') {
                    return fieldValue < val;
                }
                return true;
            });
        }, preppedData);
        return filteredPreppedData;
    }
    return preppedData;
};

const mappingHandler = async (filteredHeroes, params = [], sort = {}) => {
    const fixedPageName = filteredHeroes.map((entry) => {
        //this should fix page name
        entry.title.page = entry.title.page.replace(/&quot;/g, '"');
        return entry;
    });

    const preppedData = fixedPageName.map(({ title }) => {
        return calculateMaxStats(title);
    });
    const filteredPreppedData = advancedFilter(preppedData, params).sort((entrya, entryb) => {
        const { field, sortDirection } = sort;
        const vala = entrya[fieldMappings[field]];
        const valb = entryb[fieldMappings[field]];
        if (sortDirection === 'asc') {
            return Number(vala) - Number(valb);
        } else if (sortDirection === 'dsc') {
            return Number(valb) - Number(vala);
        }
        return 0;
    });
    if (filteredPreppedData.length === 0) {
        return { zeroResults: true };
    }
    // time to get the list of titles needed
    const unitNames = filteredPreppedData.map(({ entry: { page } }) => {
        return page;
    });
    const imagesData = await getUnitImageWithCaching(unitNames);
    //enrichment time
    const enrichedData = filteredPreppedData.map((unitData) => {
        const image = imagesData.find(({ title }) => {
            return title === unitData.entry.page;
        })?.original?.source;
        return {
            ...unitData,
            image
        };
    });
    return { enrichedData };
};

module.exports = { mappingHandler };
