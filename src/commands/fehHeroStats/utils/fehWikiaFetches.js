const fetch = require('node-fetch');
const CacheManager = require('../../../utils/cacheManager/inMemoryCacheManager');
const cacheManager = new CacheManager();
// one week expiry for images
const expiresValue = 1000 * 60 * 60 * 24 * 14;

const cachedName = 'allfehdata';
const cachedExpiry = 86400000;
const baseURL = `https://feheroes.fandom.com/api.php?format=json`;
const cargoQueryAction = 'action=cargoquery';
const tables = `tables=UnitStats,Units`;
const fields =
    "fields=Units.WikiName=WikiName,Units._pageName=page,IFNULL(CONCAT(Name,': ',Title),Name)=name,Entries,MoveType,WeaponType,Lv1HP5,Lv1Atk5,Lv1Spd5,Lv1Def5,Lv1Res5,HPGR3,AtkGR3,SpdGR3,DefGR3,ResGR3,ReleaseDate,Properties__full";
const joinOn = 'join_on=UnitStats.WikiName=Units.WikiName';
const where = `where=IFNULL(Properties__full,'') NOT LIKE '%enemy%'`;
const groupBy = 'group_by=Units.WikiName';
const orderBy = 'order_by=name';
const limit = 'limit=500';
const offset = 'offset=500';

const queryAction = 'action=query';
const jsonFormat = 'format=json';
const propImages = 'prop=pageimages';
const titleGen = (titles) => {
    return `titles=${titles.join('|')}`;
};
const formatVersion = `formatversion=2`;
const pageImagesOriginal = 'piprop=original';
const fetchAllUnitData = async () => {
    const url1 = [baseURL, cargoQueryAction, tables, fields, joinOn, where, groupBy, orderBy, limit].join('&');
    const url2 = [baseURL, cargoQueryAction, tables, fields, joinOn, where, groupBy, orderBy, limit, offset].join('&');
    const unitList = await [url1, url2].reduce(async (acc, url) => {
        const priorAcc = await acc;
        const results = await fetch(url);
        const { cargoquery } = await results.json();

        return [...priorAcc, ...cargoquery];
    }, []);
    return unitList;
};

const getUnitImage = async (unitNames) => {
    const url = [
        baseURL,
        queryAction,
        titleGen(unitNames),
        jsonFormat,
        formatVersion,
        propImages,
        pageImagesOriginal
    ].join('&');
    //console.log(url);
    const result = await fetch(url);
    const body = await result.json();
    // https://feheroes.fandom.com/api.php
    if (body.error) {
        throw new Error(JSON.stringify(body.error));
    }
    return body?.query?.pages || [];
};

const getUnitImageWithCaching = async (unitNames) => {
    // so what we have is a list of unit names that need unit names
    /*
        cache key is the unitName

        object is the page entry
    */
    const { cachedValues, neededValues } = unitNames.reduce(
        (acc, name) => {
            const { item, expired } = cacheManager.getCachedItem(name);
            if (item && !expired) {
                acc.cachedValues.push(item);
            } else {
                acc.neededValues.push(name);
            }
            return acc;
        },
        { cachedValues: [], neededValues: [] }
    );
    //split this into batches of 10
    if (neededValues.length === 0) {
        return cachedValues;
    }
    const chunkedImagesRequired = chunk(neededValues, 40);
    const fetchedImages = await chunkedImagesRequired.reduce(async (acc, neededImageWords) => {
        const awaitedAcc = await acc;
        const images = await getUnitImage(neededImageWords);

        return [...awaitedAcc, ...images];
    }, []);
    fetchedImages.forEach((imageObject) => {
        cacheManager.registerCacheItem(imageObject.title, imageObject, { expiresIn: expiresValue });
    });
    return [...cachedValues, ...fetchedImages];
};

const chunk = (arr, len) => {
    const chunks = [];
    let i = 0;
    const n = arr.length;
    while (i < n) {
        chunks.push(arr.slice(i, (i += len)));
    }
    return chunks;
};

module.exports = { fetchAllUnitData, getUnitImage, getUnitImageWithCaching, cachedName, cachedExpiry };
