//this is going to be annoying
const { helpRegister } = require('../domain/statics');

const pickit = (args) => {
    //so we are give a list of args from the handler, these are just split on a white space...which sucks so here we go
    let rejoin = args.join(' '); //try to recreate this as best as we can

    //fun we get to rejoin and go through and parse this garbage
    const delimiter = '"';

    //okay we can get multi word options like "dank memes" "wow these things here"

    //enforement time we need an even number of ""'s
    //oh god emojis will suck too uhhhhh
    const numberOfDelims = [...rejoin].reduce((acc, chr) => {
        // eslint-disable-next-line no-param-reassign
        if (chr === delimiter) acc++;
        return acc;
    }, 0);

    if (numberOfDelims % 2 !== 0) {
        //bad user you should feel ashamed we need an even number of delims
        return 'Am I joke to you? Quotes need to all be closed';
    }
    let selectableValues = [];
    //happy case we can match our delims and remove quoted words yay!
    for (let i = numberOfDelims; i > 0; i = i - 2) {
        //find a delim position 1
        const firstPos = rejoin.indexOf(delimiter);
        const secondPosition = rejoin.slice(firstPos + 1).indexOf(delimiter);

        //now we take the substring from first position to secondposition + 1 + 1
        const selectedValue = rejoin.slice(firstPos, firstPos + secondPosition + 2);
        selectableValues.push(selectedValue);

        //now to replace the substring with a single...space
        rejoin = `${rejoin.slice(0, firstPos)} ${rejoin.slice(firstPos + secondPosition + 2)}`;
    }
    //now that our delimited words are removed we can split on space our SECOND delimtered
    selectableValues = [...selectableValues, ...rejoin.split(' ')].filter((a) => a);
    const limit = selectableValues.length;
    if (limit) {
        const pickedValue = selectableValues[Math.floor(Math.random() * limit)];
        return `I choose: ${pickedValue}`;
    } else {
        return 'You passed me, Diavolo, such garbage?';
    }
};

pickit.key = 'pickit';
helpRegister(
    'pickit',
    'Ever want to have a robot pick from a list of things where some are quoted? Well I can with the pickit <list of things to pick from>'
);
const triggerWords = ['pickit'];
const constructReplyStrategy = ({ long2000Characters, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: pickit,
        appendStrategy: appendUserMentionToFront,
        splitStrategy: long2000Characters
    });
};
const mappings = triggerWords.reduce((acc, key) => {
    acc[key] = constructReplyStrategy;
    return acc;
}, {});

module.exports = {
    pickit,
    mappings
};
