//this is a simple command that posts a mp4 link
const congrats = () => {
    //note this is saved on the test discord for diavolo
    return 'https://cdn.discordapp.com/attachments/727723472024764527/819331534414807070/nice_cock.mp4';
};
const appendStrategy = ({ reply }) => `${reply}`;

congrats.key = 'congrats';
const congratsTriggers = ['congrats'];
const congratsConstruct = ({ args, id, appendUserMentionToFront, replyStrategy, userReplyMention }) => {
    return replyStrategy({
        userReplyMention,
        strategyReplyFunction: congrats,
        appendStrategy,
        splitStrategy: (msg) => [msg]
    });
};
const congratsMappings = congratsTriggers.reduce((acc, key) => {
    acc[key] = congratsConstruct;
    return acc;
}, {});

const mappings = { ...congratsMappings };
module.exports = {
    congrats,
    mappings
};
