FROM node:19-alpine
#Create the bot's dir
RUN mkdir -p /usr/src/bot
WORKDIR /usr/src/bot
COPY . /usr/src/bot
RUN npm ci	
CMD ["npm", "run", "start:docker"]
